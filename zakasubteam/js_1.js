    <!-- BEGIN .item -->
            <div class="item">
              <div class="item-header">
                <a href="<?php echo get_term_link( $show->slug, $show->taxonomy );?>"  class="img-hover-effect">
                  <img src="<?php echo wp_get_attachment_url( get_term_meta( $show->term_id,'thumbnail',true)); ?> " width="16" height="9" class="aspect-px" rel="" alt="" style="background-image:url(https://media.clhcdn.net/media/image/id/5abbcc650df9383d2a890b39.jpeg)"><span class="hoveringel" style="line-height: 118px; font-size: 35.7576px;"><i class="fa fa-play-circle"></i></span>
                </a>
              </div>
              <div class="item-content">
                <h3>
                  <a href="<?php echo get_term_link( $show->slug, $show->taxonomy );?>">
                    <i class="fa fa-play"></i><?php echo $show->name; ?></a>
                </h3>
              </div>
            <!-- END .item -->
            </div>