<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

function zakasubteam_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/zakasubteam
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'zakasubteam' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'zakasubteam' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'zakasubteam-show-cover', 1170, 300, true );
	add_image_size( 'zakasubteam-post-thumbnail', 324, 160, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'zakasubteam' ),
		'social' => __( 'Social Links Menu', 'zakasubteam' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
		add_theme_support( 'custom-logo', array(
			'flex-width'  => true,
		) );


	add_filter( 'nav_menu_css_class', 'menu_item_classes', 10, 4 );

function menu_item_classes( $classes, $item, $args, $depth ) {


    $classes[] = 'td-menu-item';

    return $classes;
}



	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

	function special_nav_class ($classes, $item) {
	    if (in_array('current-menu-item', $classes) ){
	        $classes[] = 'active ';
	    }
	    return $classes;
	}
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );


	
}
add_action( 'after_setup_theme', 'zakasubteam_setup' );



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
/**
 * Enqueue scripts and styles.
 */
function zakasubteam_scripts() {
	// Add custom fonts, used in the main stylesheet.

	// Theme require files

	wp_register_style( 'swiper-css',  get_template_directory_uri() . '/assets/css/swiper.min.css');
	wp_register_style( 'custom-css',  get_template_directory_uri() . '/assets/css/custom.css','all');
	wp_register_style( 'custom-2-css',  get_template_directory_uri() . '/assets/css/custom-2.css','all');
	wp_enqueue_style( 'swiper-css' );
	wp_enqueue_style( 'custom-css' );
	wp_enqueue_style( 'custom-2-css' );
	// Lightbox2

	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.js' );
	wp_enqueue_script( 'jquery-migrate',get_template_directory_uri() . '/assets/js/jquery.min.js' );
	wp_enqueue_script( 'jquery-easing','https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js' );
	wp_enqueue_script( 'jquery-zaccordion',get_template_directory_uri() . '/assets/js/jquery.zaccordion.js' );
	wp_enqueue_script( 'tagDivmin',get_template_directory_uri() . '/assets/js/tagDiv.min.js' );
	wp_enqueue_script( 'isotope-js','https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js' );
	wp_enqueue_script( 'swiper-js',get_template_directory_uri() . '/assets/js/swiper.min.js'  );
	wp_enqueue_script( 'lazy-js',get_template_directory_uri() . '/assets/js/jquery.lazy.min.js'  );
	wp_enqueue_script( 'custom-js',get_template_directory_uri() . '/assets/js/custom.js'  );
	
}

add_action( 'wp_enqueue_scripts', 'zakasubteam_scripts' );

// REDUX
if (!class_exists('ReduxFramewrk')) {
    require_once( dirname(__FILE__) . '/ReduxFramework/redux-framework.php' );
}
if (!isset($omazzz)) {
    require_once( dirname(__FILE__) . '/ReduxFramework/sample/sample-config.php');
}

add_action( 'widgets_init', 'main_sidebar' );
function main_sidebar() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'main-sidebar',
        'description' => __( 'Widgets in this area will be shown on homepage', 'zakasubteam' ),
        'before_widget' => '<div class="td_block_wrap td_block_7 td_block_widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="td-block-title-wrap"><h4 class="block-title"><span class="td-pulldown-size">',
		'after_title'   => '</span></h4></div>',
    ));
}

add_action( 'pre_get_posts', function ( $q ) {
    if (    $q->is_home() && $q->is_main_query() ) {
        $q->set( 'posts_per_page', 10 );
        $q->set( 'post_type', 'mvpv');
    }
});

add_action( 'pre_get_posts', function ( $q ) {
    if ($q->is_main_query() && is_search() ) {
        $q->set( 'posts_per_page', 10 );
		$q->set( 'post_type', array('mvpv','film','show','performance'));
		$q->set('paged', ( get_query_var('paged') ) ? get_query_var('paged') : 1 );

    }
});

// add_action( 'pre_get_posts', function ( $q ) {
//     if ($q->is_tax() ) {
//         $q->set( 'posts_per_page', 10 );
// 		$q->set( 'post_type', array('mvpv','film','show','performance'));
//     }
// });


require get_parent_theme_file_path( '/inc/mvpv.php' );
require get_parent_theme_file_path( '/inc/performance.php' );
require get_parent_theme_file_path( '/inc/show.php' );
require get_parent_theme_file_path( '/inc/film.php' );
require get_parent_theme_file_path( '/inc/zaka-walker.php' );
require get_parent_theme_file_path( '/inc/banner.php' );
require get_parent_theme_file_path( '/inc/widget/most-views.php' );
require get_parent_theme_file_path( '/inc/widget/facebook-fanpage.php' );

