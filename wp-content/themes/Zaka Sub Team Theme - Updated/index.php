<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
      <div class="td-header-wrap td-header-style-11 ">
          <div class="td-banner-wrap-full td-logo-wrap-full td-logo-mobile-loaded td-container-wrap ">
          <div class="banner-slider">
            <?php
             $banners = new WP_Query(array(
                "post_type" => "banner",
                "post_status" => "publish",
                "ignore_sticky_posts" => false,
                "posts_per_page" => -1,
            ));
            $banner_count = 0 ;
          ?>
              <ul id="featured">
                <?php 
                  if($banners->have_posts()) :
                    $banner_count = $banners->post_count;
                     while ( $banners->have_posts() ) : $banners->the_post();
                ?>
                <li class="slider">
                    <a href="<?php echo get_post_meta(get_the_ID(),'ref_link',true); ?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID());?>" >                  </a>
                    <div class="slider-bg"></div>
                    <div class="slider-info">
                      <strong><?php the_title(); ?></strong>
                      <p class="slider-text"><?php echo get_post_meta(get_the_ID(),'banner_desc',true); ?></p>
                    </div>
                </li>
                <?php 
                  endwhile;
                endif;
                wp_reset_postdata();
                ?>
              </ul>
              <ul id="thumbs">
              <?php 
                $active_dot = 'active-dot';
                for($i=0;$i<$banner_count;$i++):
                  if($i!=0) {
                    $active_dot = '';
                  } 
              ?>
                <li><a href="#"><span class="dot <?php echo $active_dot; ?>"></span></a></li>
                <?php endfor; ?>
           </ul>
          </div>
      </div>
      <?php 
	get_template_part('template-parts/page/index'); 
?>

<?php get_footer();
