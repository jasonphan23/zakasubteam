<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
global $zaka;
$body_bgd =  isset($zaka['background-img']) ? $zaka['background-img']['url'] : '';
$search_bgd =  isset($zaka['menu-background-img']) ? $zaka['menu-background-img']['url'] : '';
$menu_bgd = isset($zaka['search-background-img']) ? $zaka['search-background-img']['url'] : '';

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

  <head>
    <meta charset="UTF-8"/>
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <title>
      <?php wp_title(); ?>
    </title>
    <meta name="description" content=""/>
    <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script><script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]-->
    <?php wp_head(); ?>
     <script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>
        <script>var td_is_safari=false;var td_is_ios=false;var td_is_windows_phone=false;var ua=navigator.userAgent.toLowerCase();var td_is_android=ua.indexOf('android')>-1;if(ua.indexOf('safari')!=-1){if(ua.indexOf('chrome')>-1){}else{td_is_safari=true;}}if(navigator.userAgent.match(/(iPhone|iPod|iPad)/i)){td_is_ios=true;}if(navigator.userAgent.match(/Windows Phone/i)){td_is_windows_phone=true;}if(td_is_ios||td_is_safari||td_is_windows_phone||td_is_android){if(top.location!=location){top.location.replace('https://demo.tagdiv.com/newspaper/');}}var tdBlocksArray=[];function tdBlock(){this.id='';this.block_type=1;this.atts='';this.td_column_number='';this.td_current_page=1;this.post_count=0;this.found_posts=0;this.max_num_pages=0;this.td_filter_value='';this.is_ajax_running=false;this.td_user_action='';this.header_color='';this.ajax_pagination_infinite_stop='';}(function(){var htmlTag=document.getElementsByTagName("html")[0];if(navigator.userAgent.indexOf("MSIE 10.0")>-1){htmlTag.className+=' ie10';}if(!!navigator.userAgent.match(/Trident.*rv\:11\./)){htmlTag.className+=' ie11';}if(navigator.userAgent.indexOf("Edge")>-1){htmlTag.className+=' ieEdge';}if(/(iPad|iPhone|iPod)/g.test(navigator.userAgent)){htmlTag.className+=' td-md-is-ios';}var user_agent=navigator.userAgent.toLowerCase();if(user_agent.indexOf("android")>-1){htmlTag.className+=' td-md-is-android';}if(-1!==navigator.userAgent.indexOf('Mac OS X')){htmlTag.className+=' td-md-is-os-x';}if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())){htmlTag.className+=' td-md-is-chrome';}if(-1!==navigator.userAgent.indexOf('Firefox')){htmlTag.className+=' td-md-is-firefox';}if(-1!==navigator.userAgent.indexOf('Safari')&&-1===navigator.userAgent.indexOf('Chrome')){htmlTag.className+=' td-md-is-safari';}if(-1!==navigator.userAgent.indexOf('IEMobile')){htmlTag.className+=' td-md-is-iemobile';}})();var tdLocalCache={};(function(){"use strict";tdLocalCache={data:{},remove:function(resource_id){delete tdLocalCache.data[resource_id];},exist:function(resource_id){return tdLocalCache.data.hasOwnProperty(resource_id)&&tdLocalCache.data[resource_id]!==null;},get:function(resource_id){return tdLocalCache.data[resource_id];},set:function(resource_id,cachedData){tdLocalCache.remove(resource_id);tdLocalCache.data[resource_id]=cachedData;}};})();var tds_login_sing_in_widget="show";var td_viewport_interval_list=[{"limitBottom":767,"sidebarWidth":228},{"limitBottom":1018,"sidebarWidth":300},{"limitBottom":1140,"sidebarWidth":324}];var td_animation_stack_effect="type0";var tds_animation_stack=true;var td_animation_stack_specific_selectors=".entry-thumb, img";var td_animation_stack_general_selectors=".td-animation-stack img, .td-animation-stack .entry-thumb, .post img";var td_ajax_url="https:\/\/demo.tagdiv.com\/newspaper_recipes\/wp-admin\/admin-ajax.php?td_theme_name=Newspaper&v=8.5_d48";var td_get_template_directory_uri="https:\/\/demo.tagdiv.com\/newspaper_recipes\/wp-content\/themes\/011";var tds_snap_menu="smart_snap_always";var tds_logo_on_sticky="";var tds_header_style="11";var td_please_wait="Please wait...";var td_email_user_pass_incorrect="User or password incorrect!";var td_email_user_incorrect="Email or username incorrect!";var td_email_incorrect="Email incorrect!";var tds_more_articles_on_post_enable="";var tds_more_articles_on_post_time_to_wait="";var tds_more_articles_on_post_pages_distance_from_top=0;var tds_theme_color_site_wide="#35a533";var tds_smart_sidebar="enabled";var tdThemeName="Newspaper";var td_magnific_popup_translation_tPrev="Previous (Left arrow key)";var td_magnific_popup_translation_tNext="Next (Right arrow key)";var td_magnific_popup_translation_tCounter="%curr% of %total%";var td_magnific_popup_translation_ajax_tError="The content from %url% could not be loaded.";var td_magnific_popup_translation_image_tError="The image #%curr% could not be loaded.";var tdDateNamesI18n={"month_names":["January","February","March","April","May","June","July","August","September","October","November","December"],"month_names_short":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"day_names":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"day_names_short":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]};var td_ad_background_click_link="";var td_ad_background_click_target="";</script>
  </head>
<?php

  $body_class = array('td-recipes','td-boxed-layout');
?>
<body <?php echo body_class($body_class ); ?> style="background-image: url(<?php echo $body_bgd; ?>)">
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=340300943095151&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="td-menu-background" style="background-image: url(<?php echo $menu_bgd; ?>)" ></div>
<div class="td-scroll-up td-scroll-up-visible"><i class="fas fa-arrow-up"></i></div>
<div id="td-mobile-nav">
         <div class="td-mobile-container">
            <div class="td-menu-socials-wrap">
               <div class="td-mobile-close">
                  <a href="#"><i class="fa fa-times"></i></a>
               </div>
            </div>
            <div class="td-mobile-content">
               <?php
                $zakawalker = new Zakasubteam_Nav_Walker;
                wp_nav_menu( array(
                    'theme_location' => 'top',
                    'menu_class' => 'td-mobile-main-menu',
                    'container' => 'div',
                    'container_class' => 'menu-main-menu-container'
                ) ); 
                ?>
            </div>
         </div>
</div>
<div class="td-search-background" style="background-image: url(<?php echo $search_bgd; ?>)"></div>
      <div class="td-search-wrap-mob">
         <div class="td-drop-down-search" aria-labelledby="td-header-search-button">
            <form method="get" class="td-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
               <div class="td-search-close">
                  <a href="#"><i class="fa fa-times"></i></a>
               </div>
               <div role="search" class="td-search-input">
                  <span>Search</span>
                  <input id="td-header-search-mob" type="text" value="" name="s" autocomplete="off">
               </div>
            </form>
            <div id="td-aj-search-mob"></div>
         </div>
</div>
<div id="td-outer-wrap" class="td-theme-wrap">
  <div class="td-header-wrap td-header-style-11 ">    
    <?php get_template_part('template-parts/navigation/header','navigation'); ?>