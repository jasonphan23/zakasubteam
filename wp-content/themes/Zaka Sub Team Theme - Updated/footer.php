<?php
    global $zaka;
    $header_lg =  isset($zaka['header-logo']) ? $zaka['header-logo']['url'] : '';
    $site_title =  isset($zaka['site-title']) ? $zaka['site-title']: '';
    $site_title =  isset($zaka['site-title']) ? $zaka['site-title']: '';
    $footer_title =  isset($zaka['footer-title']) ? $zaka['footer-title']: '';
    $footer_text =  isset($zaka['footer-text']) ? $zaka['footer-text']: '';
?>
<div class="td-footer-wrapper td-container-wrap ">
   <div class="td-footer-bottom-full">
      <div class="td-container">
         <div class="td-pb-row">
            <div class="td-pb-span4">
               <aside class="footer-logo-wrap"><a href="<?php home_url(); ?>"><img class="td-retina-data" src="<?php echo $header_lg;  ?>" alt="<?php echo $site_title ?>" title="<?php echo $site_title ?>" width="231"></a></aside>
            </div>
            <div class="td-pb-span7">
               <aside class="footer-text-wrap">
                  <div class="block-title"><span><?php echo $footer_title;  ?></span></div>
                  <span><?php echo $footer_text;  ?></span>
               </aside>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="td-sub-footer-container td-container-wrap ">
<div class="td-container">
<div class="td-pb-row">
<div class="td-pb-span td-sub-footer-menu">
<div class="td-pb-span td-sub-footer-copy">
© Theme by Zakasubteam </div>
</div>
</div>
</div>