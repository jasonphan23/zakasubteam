<?php

add_action('init', 'create_home_banner_post_type',0);
function create_home_banner_post_type() {
	// REGISTER EXHIBITION CUSTOM POST TYPE
	register_post_type( 'banner',
	array(
		'labels' => array(
			'name' => 'Banner',
			'singular_name' => 'Banner',
			'add_new_item' => 'Add New Banner',
			'edit' => 'Edit',
			'edit_item' => 'Edit Banner',
			'new_item' => 'New Banner',
			'view' => 'View',
			'view_item' => 'View Banner',
			'search_items' => 'Search Banner',
			'not_found' => 'No Banner found',
			'not_found_in_trash' => 'No Banner found in Trash',
			'parent' => 'Parent Banner'
		),
		//THIS POST TYPE IS ONLY ACCESSIBLE FOR ADMIN . UPDATE_CORE IS FOR ADMIN ONLY CAPABILITY
		/* 'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		), */
		'public' => true,
		'menu_position' => 99,
		'supports' => array( 'title', 'thumbnail'),
		'has_archive' => true,
		'Banner_ui' => true,
		'Banner_in_menu' => true,
		'Banner_in_nav_menus' => true,
		'Banner_in_admin_bar' => true,
		'Banner_in_rest' => true,
		'menu_icon' => 'dashicons-video-alt2'
	));
}

//Banner ATTRIBUTES META

if( function_exists('acf_add_local_field_group') ):
	acf_add_local_field_group(array(
	'key' => 'group_5b348be60f1a3',
	'title' => 'Banner',
	'fields' => array(
		array(
			'key' => 'field_5b348be8e636b',
			'label' => 'Ref Link',
			'name' => 'ref_link',
			'type' => 'url',
			'instructions' => 'Đường dẫn khi click vào banner',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array(
			'key' => 'field_5b348be8e636b',
			'label' => 'Đương dẫn',
			'name' => 'ref_link',
			'type' => 'url',
			'instructions' => 'Đường dẫn khi click vào banner',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array(
			'key' => 'field_5b49a44de81ff',
			'label' => 'Mô tả',
			'name' => 'banner_desc',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'banner',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


?>
