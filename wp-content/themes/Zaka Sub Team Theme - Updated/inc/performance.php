<?php

add_action('init', 'create_performance_post_type',0);
function create_performance_post_type() {
	// REGISTER EXHIBITION CUSTOM POST TYPE 
	register_post_type( 'performance',
	array(
		'labels' => array(
			'name' => 'Performance',
			'singular_name' => 'Performance',
			'add_new_item' => 'Add New Performance',
			'edit' => 'Edit',
			'edit_item' => 'Edit Performance',
			'new_item' => 'New Performance',
			'view' => 'View',
			'view_item' => 'View Performance',
			'search_items' => 'Search Performance',
			'not_found' => 'No Performance found',
			'not_found_in_trash' => 'No Performance found in Trash',
			'parent' => 'Parent Performance'
		),
		//THIS POST TYPE IS ONLY ACCESSIBLE FOR ADMIN . UPDATE_CORE IS FOR ADMIN ONLY CAPABILITY
		/* 'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		), */
		'public' => true,
		'menu_position' => 99,
		'supports' => array( 'title', 'thumbnail','editor',"page-attributes"),
		'has_archive' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'show_in_nav_menus' => true, 
		'show_in_admin_bar' => true, 
		'show_in_rest' => true,
		'menu_icon' => 'dashicons-format-audio',
		'taxonomies' => array('post_tag')
	));
}

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5b4e14d86ce1c',
		'title' => 'Performance',
		'fields' => array(
			
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'performance',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	endif;

?>