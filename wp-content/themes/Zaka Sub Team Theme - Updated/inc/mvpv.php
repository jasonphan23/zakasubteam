<?php

add_action('init', 'create_mvpv_post_type',0);
function create_mvpv_post_type() {
	// REGISTER EXHIBITION CUSTOM POST TYPE 
	register_post_type( 'mvpv',
	array(
		'labels' => array(
			'name' => 'MV & PV',
			'singular_name' => 'MV & PV',
			'add_new_item' => 'Add New MV & PV',
			'edit' => 'Edit',
			'edit_item' => 'Edit MV & PV',
			'new_item' => 'New MV & PV',
			'view' => 'View',
			'view_item' => 'View MV & PV',
			'search_items' => 'Search MV & PV',
			'not_found' => 'No MV & PV found',
			'not_found_in_trash' => 'No MV & PV found in Trash',
			'parent' => 'Parent MV & PV'
		),
		//THIS POST TYPE IS ONLY ACCESSIBLE FOR ADMIN . UPDATE_CORE IS FOR ADMIN ONLY CAPABILITY
		/* 'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		), */
		'public' => true,
		'menu_position' => 99,
		'supports' => array( 'title', 'thumbnail','editor',"page-attributes"),
		'has_archive' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'show_in_nav_menus' => true, 
		'show_in_admin_bar' => true, 
		'show_in_rest' => true,
		'menu_icon' => 'dashicons-format-video',
		'rewrite'  => array( 'slug' => 'mvpv' ),
		'taxonomies' => array('post_tag')
	));
}

function create_single_category_taxonomy() {
	$labels = array(
		'name' => 'Single',
		'singular' => 'Single',
		'menu_name' => 'Single'
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' => array('slug' => 'single', 'with_front' => false),
	);
	register_taxonomy('single', 'mvpv', $args);
}
add_action( 'init', 'create_single_category_taxonomy', 0 );

function create_mvpv_type_taxonomy() {
	$labels = array(
		'name' => 'MV & PV Type',
		'singular' => 'MV & PV Type',
		'menu_name' => 'MV & PV Type'
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' => array('slug' => 'mvpvtype'),
	);
	register_taxonomy('mvpvtype', 'mvpv', $args);
}
add_action( 'init', 'create_mvpv_type_taxonomy', 0 );



if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5b1ac13cb4e20',
	'title' => 'Taxonomy Single',
	'fields' => array(
		array(
			'key' => 'field_5b1ac14347385',
			'label' => 'Ảnh đại diện',
			'name' => 'thumbnail',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'single',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5b1ac18c8df57',
	'title' => 'MV and PV',
	'fields' => array(
		array(
			'key' => 'field_5b1ac18fe74d5',
			'label' => 'Link Google Drive',
			'name' => 'gg_drive_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'mvpv',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


endif;



?>