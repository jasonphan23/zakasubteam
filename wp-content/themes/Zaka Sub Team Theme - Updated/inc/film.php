<?php

add_action('init', 'create_film_post_type',0);
function create_film_post_type() {
	// REGISTER EXHIBITION CUSTOM POST TYPE
	register_post_type( 'film',
	array(
		'labels' => array(
			'name' => 'Film',
			'singular_name' => 'Film',
			'add_new_item' => 'Add New Film',
			'edit' => 'Edit',
			'edit_item' => 'Edit Film',
			'new_item' => 'New Film',
			'view' => 'View',
			'view_item' => 'View Film',
			'search_items' => 'Search Film',
			'not_found' => 'No Film found',
			'not_found_in_trash' => 'No Film found in Trash',
			'parent' => 'Parent Film'
		),
		//THIS POST TYPE IS ONLY ACCESSIBLE FOR ADMIN . UPDATE_CORE IS FOR ADMIN ONLY CAPABILITY
		/* 'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		), */
		'public' => true,
		'menu_position' => 99,
		'supports' => array( 'title', 'thumbnail','editor',"page-attributes"),
		'has_archive' => true,
		'film_ui' => true,
		'film_in_menu' => true,
		'film_in_nav_menus' => true,
		'film_in_admin_bar' => true,
		'film_in_rest' => true,
		'menu_icon' => 'dashicons-video-alt2',
		'taxonomies' => array('post_tag')
	));
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5b49b6c7ee1f1',
	'title' => 'Film',
	'fields' => array(

	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'film',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>
