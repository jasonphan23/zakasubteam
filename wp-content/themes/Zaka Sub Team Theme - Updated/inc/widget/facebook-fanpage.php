<?php 
class FanpageFacebook extends WP_Widget {
 
    function __construct() {
        parent::__construct(
            'fb_fanpage',
            'Trang nhóm',
            array( 'description'  =>  'Trang nhóm Facebook' )
        );
    }
 
    function form( $instance ) {
 
        $default = array(
            'title' => 'Trang nhóm',
            'fanpage' => 'https://www.facebook.com/zakasubteam/',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $fanpage = esc_attr($instance['fanpage']);
        $title = esc_attr($instance['title']);
        echo '<p>Nhập tiêu đề <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Nhập vào trang nhóm <input type="text" class="widefat" name="'.$this->get_field_name('fanpage').'" value="'.$fanpage.'"/></p>';
    }
 
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['fanpage'] = strip_tags($new_instance['fanpage']);
        return $instance;
    }
 
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $fanpage = $instance['fanpage'];
 
        echo $before_widget;
        echo $before_title.$title.$after_title;
        ?>
        <div class="fb-page"
          data-href="<?php echo $fanpage ?>" 
          data-width="340"
          data-hide-cover="false"
          data-show-facepile="true">  
        </div>

        <?php
        echo $after_widget;
    }
 
}


 
add_action( 'widgets_init', 'create_fanpage_fb_widget' );
function create_fanpage_fb_widget() {
    register_widget('FanpageFacebook');
}

?>