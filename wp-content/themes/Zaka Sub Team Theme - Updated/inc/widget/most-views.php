<?php 
class MostViews extends WP_Widget {
 
    function __construct() {
        parent::__construct(
            'most_view',
            'Xem nhiều nhất',
            array( 'description'  =>  'Các bài viết được xem nhiều nhất' )
        );
    }
 
    function form( $instance ) {
 
        $default = array(
            'title' => 'Tiêu đề',
            'post_number' => 3
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $post_number = esc_attr($instance['post_number']);
 
        echo '<p>Nhập tiêu đề <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Số lượng bài viết hiển thị <input type="number" class="widefat" name="'.$this->get_field_name('post_number').'" value="'.$post_number.'" placeholder="'.$post_number.'" max="10" /></p>';
 
    }
 
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['post_number'] = strip_tags($new_instance['post_number']);
        return $instance;
    }
 
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $post_number = $instance['post_number'];
 
        echo $before_widget;
        echo $before_title.$title.$after_title;
        $most_views = new WP_Query(
            array(
           "post_type" => array("show","performance","mvpv"),
           "post_status" => "publish",
           "ignore_sticky_posts" => false,
           "posts_per_page" => $post_number,
            'meta_key' => 'view_num',
            'orderby'    => 'meta_value_num',
            'order'      => 'DESC',
         ));
        


        if ($most_views->have_posts()):
            while( $most_views->have_posts() ) :
                $most_views->the_post(); ?>
            <div class="td-block-span12">
                <div class="td_module_6 td_module_wrap td-animation-stack">
                    <div class="td-module-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img width="100" height="70" class="entry-thumb td-animation-stack-type0-1" src="<?php  echo get_the_post_thumbnail_url()?> " alt="" title="<?php the_title() ?> "></a>
                    </div>
                    <div class="item-details">
                           <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Rocky Road Bites"><?php the_title(); ?></a></h3>
                           <div class="td-module-meta-info">
                              <a href="<?php the_permalink(); ?>" class="td-post-category"><?php the_title(); ?></a>
                           </div>
                    </div>
                 </div>
             </div>
            <?php endwhile;
        endif;
        echo $after_widget;
    }
 
}


 
add_action( 'widgets_init', 'create_most_views_widget' );
function create_most_views_widget() {
    register_widget('MostViews');
}

?>