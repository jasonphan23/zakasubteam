<?php

add_action('init', 'create_show_post_type',0);
function create_show_post_type() {
	// REGISTER EXHIBITION CUSTOM POST TYPE
	register_post_type( 'show',
	array(
		'labels' => array(
			'name' => 'Show',
			'singular_name' => 'Show',
			'add_new_item' => 'Add New Show',
			'edit' => 'Edit',
			'edit_item' => 'Edit Show',
			'new_item' => 'New Show',
			'view' => 'View',
			'view_item' => 'View Show',
			'search_items' => 'Search Show',
			'not_found' => 'No Show found',
			'not_found_in_trash' => 'No Show found in Trash',
			'parent' => 'Parent Show'
		),
		//THIS POST TYPE IS ONLY ACCESSIBLE FOR ADMIN . UPDATE_CORE IS FOR ADMIN ONLY CAPABILITY
		/* 'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		), */
		'public' => true,
		'menu_position' => 99,
		'supports' => array( 'title', 'thumbnail','editor',"page-attributes"),
		'has_archive' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_rest' => true,
		'menu_icon' => 'dashicons-video-alt2',
		'taxonomies' => array('post_tag')
	));
}

//SHOW ATTRIBUTES META

if( function_exists('acf_add_local_field_group') ):
acf_add_local_field_group(array(
	'key' => 'group_5b08e7be3e776',
	'title' => 'Show',
	'fields' => array(
		array(
			'key' => 'field_5b08e7c937db1',
			'label' => 'Tập',
			'name' => 'episode',
			'type' => 'number',
			'instructions' => 'Nhập vào tập của show (Bắt buộc)',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'Nhập vào tập của show ở đây',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5b1247779449f',
			'label' => 'Link Google Drive',
			'name' => 'gg_drive_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5b1247ab944a0',
			'label' => 'Dailymotion Link',
			'name' => 'dailymotion_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5b1247c2944a1',
			'label' => '4File Link',
			'name' => '4file_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'show',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


endif;

//CREATE SHOW TAXONOMY

function create_show_category_taxonomy() {
	$labels = array(
		'name' => 'Show Categories',
		'singular' => 'Show Category',
		'menu_name' => 'Show Category'
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' => array('slug' => 'show_category', 'with_front' => false),
	);
	register_taxonomy('show_category', 'show', $args);
}
add_action( 'init', 'create_show_category_taxonomy', 0 );



if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5afcffa7ade2d',
	'title' => 'Taxonomy Show',
	'fields' => array(
		array(
			'key' => 'field_5afcffae3905b',
			'label' => 'Ngày Công Chiếu',
			'name' => 'starting_date',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'd/m/Y',
			'return_format' => 'd/m/Y',
			'first_day' => 1,
		),
		array(
			'key' => 'field_5afd00c03905c',
			'label' => 'Độ dài',
			'name' => 'length',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5afd022b5011d',
			'label' => 'Ảnh đại diện',
			'name' => 'thumbnail',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5afd02675011e',
			'label' => 'Ảnh bìa',
			'name' => 'cover_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'zakasubteam-show-cover',
			'library' => 'all',
			
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5affa5878f6dc',
			'label' => 'Tags',
			'name' => 'tags',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'post_tag',
			'field_type' => 'multi_select',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),
		array(
			'key' => 'field_5affb10a611d8',
			'label' => 'Thể Loại',
			'name' => 'genre',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'show_genre',
			'field_type' => 'checkbox',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),
		array(
			'key' => 'field_5b3db90777301',
			'label' => 'Hiện Ở Trang Chủ',
			'name' => 'show_home',
			'type' => 'true_false',
			'instructions' => 'Cho show này xuất hiện ở trang chủ hay không',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5b3db92d77302',
			'label' => 'Thứ tự xuất hiện',
			'name' => 'home_order',
			'type' => 'number',
			'instructions' => 'Thứ tự xuất hiện của show . Nhập số càng nhỏ thì càng xuất hiện trước',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5b3db90777301',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5b3db95d77303',
			'label' => 'Màu nền',
			'name' => 'label_color',
			'type' => 'color_picker',
			'instructions' => 'Màu nền của show',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5b3db90777301',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
	),	

	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'show_category',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


//CREATE SHOW GENRES

function create_show_genres_taxonomy() {
	$labels = array(
		'name' => 'Show Genres',
		'singular' => 'Show Genre',
		'menu_name' => 'Show Genre'
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' => array('slug' => 'show_genre', 'with_front' => false),
	);
	register_taxonomy('show_genre', 'show', $args);
}
add_action( 'init', 'create_show_genres_taxonomy', 0 );

function add_ajax_function($function, $callback) {
    $arrAjaxType = array(
        'wp_ajax_',
        'wp_ajax_nopriv_',
    );

    foreach ($arrAjaxType as $type) {
        add_action($type . $function, $callback);
    }
}

add_ajax_function('list_shows', 'get_list_show');

function get_list_show() {
	$page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
	
    if (!empty($page)) {
        $result = 1;
        $content = get_show_list_template($page);
    } else {
        $result = 0;
        $content = '';
    }
    $data = array(
        'result' => $result,
        'content' => $content,
    );
    echo json_encode($data);
    exit;
}

function get_show_list_template($page) {
	$output = '';
    $number   = 5;
	$offset       = ( $page > 0 ) ?  $number * ( $page - 1 ) : 1;
    $ajax_url = admin_url("admin-ajax.php");
    $show_list_args = array(
        'hide_empty' => 0,
        'taxonomy' => 'show_category',
        'number'        => $number, 
        'offset'        => $offset
	 );
	 $totalterms   = wp_count_terms( 'show_category', array( 'hide_empty' => FALSE) ); 
	 $totalpages   = ceil( $totalterms / $number );
	$shows_list = get_terms($show_list_args);
        if ( ! empty( $shows_list ) && ! is_wp_error( $shows_list ) ) :
                    foreach ($shows_list as $show):
                        $show_link = get_term_link( $show->slug, $show->taxonomy );
                        $show_title =  $show->name;
                        $show_thumbnail_url = wp_get_attachment_url(intval(get_term_meta($show->term_id,'thumbnail',true)));
                        $output .= '<div class="td-mega-span">
                        <div class="td_module_mega_menu td_mod_mega_menu">
                           <div class="td-module-image">
                              <div class="td-module-thumb"> <a href="' . $show_link .'" title="' . $show_title . '" rel="bookmark"><img width="218" height="150" class="entry-thumb" src="' . $show_thumbnail_url . '" alt="' . $show_title . '"></a></div>
                             
                           </div>
                           <div class="item-details">
                              <h3 class="entry-title td-module-title">
                              <a href="' . $show_link .'" >' . $show_title .  '</a>                               
                              </h3>
                           </div>
                        </div>
                     </div>';
					endforeach;
					$ajax_left_disabled = $ajax_right_disabled =  '';
					if($page==1) {
						$ajax_left_disabled = 'ajax-page-disabled';
					}
					if($page+1 > $totalpages) {
						$ajax_right_disabled = 'ajax-page-disabled';	
					}
					$prev_page = $page - 1 ;
					$next_page = $page+1;
					$output .= '<div class="td-next-prev-wrap">
                    <a href="javascript:void(0);" id="list-show-left" class="paging-show td-ajax-prev-page '. $ajax_left_disabled . '"   data-url-ajax="' . $ajax_url . '?action=list_shows&page=' . $prev_page . '">
                        <i class="fa fa-arrow-left"></i>
					</a>
					<a href="javascript:void(0);" id="list-show-right" class="paging-show list-show-right td-ajax-prev-page '. $ajax_right_disabled . '"  data-url-ajax="' . $ajax_url . '?action=list_shows&page=' . $next_page . '">
						<i class="fa fa-arrow-right"></i>
					</a>
                    </div>';
		 endif;
		 wp_reset_query();
		 return $output;
}
?>
