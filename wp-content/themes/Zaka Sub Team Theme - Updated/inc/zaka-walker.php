<?php
class Zakasubteam_Nav_Walker extends Walker_Nav_Menu {
 
 /**
 * Phương thức start_lvl()
 * Được sử dụng để hiển thị các thẻ bắt đầu cấu trúc của một cấp độ mới trong menu. (ví dụ: <ul class="sub-menu">)
 * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
 * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
 * @param array $args | Các tham số trong hàm wp_nav_menu()
 **/
 public function start_lvl( &$output, $depth = 0, $args = array() )
 {
 	 $indent = str_repeat("\t", $depth);
  $output .= "\n$indent<ul class=\"sub-menu\">\n";
 }
 
 /**
 * Phương thức end_lvl()
 * Được sử dụng để hiển thị đoạn kết thúc của một cấp độ mới trong menu. (ví dụ: </ul> )
 * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
 * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
 * @param array $args | Các tham số trong hàm wp_nav_menu()
 **/
 public function end_lvl( &$output, $depth = 0, $args = array() )
 {
   $indent = str_repeat("\t", $depth);
  $output .= "$indent</ul>\n";
 }
 
 /**
 * Phương thức start_el()
 * Được sử dụng để hiển thị đoạn bắt đầu của một phần tử trong menu. (ví dụ: <li id="menu-item-5"> )
 * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
 * @param string $item | Dữ liệu của các phần tử trong menu
 * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
 * @param array $args | Các tham số trong hàm wp_nav_menu()
 * @param interger $id | ID của phần tử hiện tại
 **/
 public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
 {
 	  $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
    
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names .'>';
 
        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
 
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
 
        $attributes = '';
        foreach ( $atts as $attr => $value ) {
                if ( ! empty( $value ) ) {
                        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                        $attributes .= ' ' . $attr . '="' . $value . '"';
                }
        }
 
        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        /** This filter is documented in wp-includes/post-template.php */
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $mega_menu = 'td-mega-menu';
        
        if (in_array($mega_menu, $classes) && !empty($classes)) {
            if ($item->attr_title == 'show') {
                $item_output .= get_show();
            }
        }
        $item_output .= $args->after;
 
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
 }
 

 
 /**
 * Phương thức end_el()
 * Được sử dụng để hiển thị đoạn kết thúc của một phần tử trong menu. (ví dụ: </li> )
 * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
 * @param string $item | Dữ liệu của các phần tử trong menu
 * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
 * @param array $args | Các tham số trong hàm wp_nav_menu()
 * @param interger $id | ID của phần tử hiện tại
 **/
 public function end_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
 {
        $output .= "</li>\n";
 }


} // end ThachPham_Nav_Walker

function get_show() {
    $output = '';
    $page = 1; 
    $number   = 5;
    $offset       = ( $page > 0 ) ?  $number * ( $page - 1 ) : 1;
    $ajax_url = admin_url("admin-ajax.php");
    $show_list_args = array(
        'hide_empty' => 0,
        'taxonomy' => 'show_category',
        'number'        => $number, 
        'offset'        => $offset
     );
     $totalterms   = wp_count_terms( 'show_category', array( 'hide_empty' => FALSE) ); 
	 $totalpages   = ceil( $totalterms / $number );
    $shows_list = get_terms($show_list_args);
        if ( ! empty( $shows_list ) && ! is_wp_error( $shows_list ) ) :
        $output .= '
        <ul class="sub-menu show-mega-menu">
            <div class="td-container-border">
                <div class="td-mega-grid">
                    <div class="show-row td-mega-row">';
                    foreach ($shows_list as $show):
                        $show_link = get_term_link( $show->slug, $show->taxonomy );
                        $show_title =  $show->name;
                        $show_thumbnail_url = wp_get_attachment_url(intval(get_term_meta($show->term_id,'thumbnail',true)));
                        $output .= '<div class="td-mega-span">
                        <div class="td_module_mega_menu td_mod_mega_menu">
                           <div class="td-module-image">
                              <div class="td-module-thumb"> <a href="' . $show_link .'" title="' . $show_title . '" rel="bookmark"><img width="218" height="150" class="entry-thumb" src="' . $show_thumbnail_url . '" alt="' . $show_title . '"></a></div>
                            
                           </div>
                           <div class="item-details">
                              <h3 class="entry-title td-module-title">
                              <a href="' . $show_link .'">' . $show_title .  '</a>                               
                              </h3>
                           </div>
                        </div>
                     </div>';
                    endforeach;
                    $disbale_right = '';
                    if( $totalpages ==1) {
                        $disbale_right = ' ajax-page-disabled';
                    }
                    $output .= '<div class="td-next-prev-wrap">
                    <a href="javascript:void(0);" id="list-show-left" class="paging-show td-ajax-prev-page ajax-page-disabled">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a href="javascript:void(0);" id="list-show-right"  class="paging-show td-ajax-next-page'.$disbale_right. '" data-url-ajax="' . $ajax_url . '?action=list_shows&page=2">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    </div>';
                    $output .= '</div></div></div></ul>';
         endif;
    wp_reset_query();
    return $output;
}
?>