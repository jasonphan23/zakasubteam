<?php
/*
  ReduxFramework Sample Config File
  For full documentation, please visit: https://github.com/ReduxFramework/ReduxFramework/wiki
 * */

if (!class_exists("Redux_Framework_sample_config")) {
    class Redux_Framework_sample_config {
        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;
        public function __construct() {
            $this->initSettings();        
        }
        public function initSettings() {

            if ( !class_exists("ReduxFramework" ) ) {
                return;
            }       
            
            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
           
            add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        
        function compiler_action($options, $css) {
            
        }

       
        function dynamic_section($sections) {        

            return $sections;
        }

        /*

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /*

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = "Testing filter hook!";

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_meta_demo_mode_link'), null, 2);
            }

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
        }

        public function setSections() {

            /*
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode(".", $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[] = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct = wp_get_theme();
            $this->theme = $ct;
            $item_name = $this->theme->get('Name');
            $tags = $this->theme->Tags;
            $screenshot = $this->theme->get_screenshot();
            $class = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redux-framework-demo'), $this->theme->display('Name'));
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                    <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                        <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'zakasubteam'); ?>" />
                    </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'zakasubteam'); ?>" />
            <?php endif; ?>

                <h4>
            <?php echo htmlspecialchars_decode($this->theme->display('Name')); ?>
                </h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'redux-framework-demo'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'redux-framework-demo'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'redux-framework-demo') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo htmlspecialchars_decode($this->theme->display('Description')); ?></p>
                <?php
                if ($this->theme->parent()) {
                    printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'redux-framework') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redux-framework-demo'), $this->theme->parent()->display('Name'));
                }
                ?>

                </div>

            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }
            
			
			$this->sections[] = array(
               // 'icon' => ' el-icon-stackoverflow',
                'title' => __('Header', 'theme_omazz'),
                'fields' => array(
					array(
                        'id'       => 'header-logo',
                        'type'     => 'media', 
                        'title'    => __('Header Logo', 'theme_zakasubteam'),
                        'default'  => ''
                    ),
                     array(
                         'id'       => 'site-title',
                         'type'     => 'text', 
                         'title'    => __('Site Title', 'theme_zakasubteam'),
                         'default'  => 'ZAKASUBTEAM',
                     ),
                     array(
                        'id'       => 'header-bgd',
                        'type'     => 'media', 
                        'title'    => __('Header Background', 'theme_zakasubteam'),
                        'default'  => ''
                    )
                )
                );
            
                $this->sections[] = array(
                    // 'icon' => ' el-icon-stackoverflow',
                     'title' => __('Body', 'theme_zakasubteam'),
                     'fields' => array(
                         array(
                             'id'       => 'background-img',
                             'type'     => 'media', 
                             'title'    => __('Background Image', 'theme_zakasubteam'),
                             'default'  => '',
                         ),
                         array(
                            'id'       => 'play-btn',
                            'type'     => 'media', 
                            'title'    => __('Play Button', 'theme_zakasubteam'),
                            'default'  => '',
                        ),
                         array(
                            'id'       => 'menu-background-img',
                            'type'     => 'media', 
                            'title'    => __('Menu Background Image (Mobile & Tablet Only)', 'theme_zakasubteam'),
                            'default'  => '',
                         ),
                         array(
                            'id'       => 'search-background-img',
                            'type'     => 'media', 
                            'title'    => __('Search Background Image (Mobile & Tablet Only)', 'theme_zakasubteam'),
                            'default'  => '',
                        )

                     )
                 );

            
             $this->sections[] = array(
                // 'icon' => ' el-icon-stackoverflow',
                 'title' => __('Tumblr API Key', 'theme_zakasubteam'),
                 'fields' => array(
                     array(
                         'id'       => 'tumblr-key',
                         'type'     => 'text',
                         'title'    => __('Tumblr API Key', 'theme_zakasubteam'),
                         'default'  => '',
                     )
                 )
             );

             $this->sections[] = array(
                // 'icon' => ' el-icon-stackoverflow',
                 'title' => __('Footer', 'theme_zakasubteam'),
                 'fields' => array(
                     array(
                         'id'       => 'footer-title',
                         'type'     => 'text',
                         'title'    => __('Footer Block 1 Title', 'theme_zakasubteam'),
                         'default'  => '',
                     ),
                     array(
                         'id'       => 'footer-text',
                         'type'     => 'editor',
                         'title'    => __('Footer Block 1 Text', 'theme_zakasubteam'),
                         'default'  => '',
                     ),
                 )
             );


			
			// $this->sections[] = array(
            //    // 'icon' => ' el-icon-stackoverflow',
            //     'title' => __('Finance Calculation', 'theme_translate'),
            //     'fields' => array(
			// 		array(
            //             'id'       => 'interested-car',
            //             'type'     => 'text', 
            //             'title'    => __('Interested', 'theme_translate'),
            //             'default'  => '7.5'
            //         ),
					
            //     )
            // );
			
			// $this->sections[] = array(
            //    // 'icon' => ' el-icon-stackoverflow',
            //     'title' => __('Site', 'theme_translate'),
            //     'fields' => array(
			// 		array(
            //             'id'       => 'compress-car-link',
            //             'type'     => 'text', 
            //             'title'    => __('Link compress car', 'theme_translate'),
            //             'default'  => 'http://mercedes-findcar.voolatech.com/so-sanh-xe/'
            //         ),
			// 		array(
            //             'id'       => 'dealer-email',
            //             'type'     => 'text', 
            //             'title'    => __('Email Dealer', 'theme_translate'),
            //             'default'  => null
            //         ),
			// 		array(
            //             'id'       => 'dealer-phone-call',
            //             'type'     => 'text', 
            //             'title'    => __('Phone Dealer', 'theme_translate'),
            //             'default'  => null
            //         ),
            //     )
            // );
			
			// $this->sections[] = array(
            //    // 'icon' => ' el-icon-stackoverflow',
            //     'title' => __('Footer', 'theme_translate'),
            //     'fields' => array(
            //         array(
            //             'id'       => 'footer-address',
            //             'type'     => 'text', 
            //             'title'    => __('Address', 'theme_translate'),
            //             'default'  => 'Công ty Mercedes - Địa chỉ: Điện Biên Phủ, Q.1, TP.HCM'
            //         ),
			// 		array(
            //             'id'       => 'footer-contact',
            //             'type'     => 'text', 
            //             'title'    => __('Contact', 'theme_translate'),
            //             'default'  => 'Email: mercedes@gmail.com - Phone: 090 855 2237'
            //         ),
			// 		array(
            //             'id'       => 'footer-copyright',
            //             'type'     => 'text', 
            //             'title'    => __('Copyright', 'theme_translate'),
            //             'default'  => 'Copyright @ 2014 By Mercedes'
            //         ),
			// 		array(
            //             'id'       => 'footer-facebook',
            //             'type'     => 'text', 
            //             'title'    => __('Facebook', 'theme_translate'),
            //             'default'  => 'http://facebook.com'
            //         ),
			// 		array(
            //             'id'       => 'footer-twitter',
            //             'type'     => 'text', 
            //             'title'    => __('Twitter', 'theme_translate'),
            //             'default'  => 'http://twitter.com'
            //         ),
			// 		array(
            //             'id'       => 'footer-google',
            //             'type'     => 'text', 
            //             'title'    => __('Google Plus', 'theme_translate'),
            //             'default'  => 'http://google.com'
            //         ),
			// 		array(
            //             'id'       => 'footer-youtube',
            //             'type'     => 'text', 
            //             'title'    => __('Youtube', 'theme_translate'),
            //             'default'  => 'http://youtube.com'
            //         ),
			// 		array(
            //             'id'       => 'footer-pinterest',
            //             'type'     => 'text', 
            //             'title'    => __('Pinterest', 'theme_translate'),
            //             'default'  => 'https://www.pinterest.com/'
            //         ),
					
            //     )
            // );
			
			// $this->sections[] = array(
            //    // 'icon' => ' el-icon-stackoverflow',
            //     'title' => __('Email Testdrive', 'mercedes'),
            //     'fields' => array(
            //         array(
            //             'id'       => 'email-testdrive-on-off',
            //             'type'     => 'switch', 
            //             'title'    => __('Email Off?', 'mercedes'),
            //             'subtitle' => __('Look, it\'s on!', 'mercedes'),
            //             'default'  => true,
            //         ),
					
			// 		array(
            //             'id'       => 'email-testdrive-title',
            //             'type'     => 'text', 
            //             'title'    => __('Title Email?', 'mercedes'),
            //             'default'  => 'Title Email',
            //         ),
			// 		array(
            //             'id'       => 'email-testdrive-messenger',
            //             'type'     => 'editor', 
            //             'title'    => __('Messenger Email?', 'mercedes'),
            //             'desc'    => '##name##: Name User Submit.<br> ##email##: Email User Submit<br>##phone##: Số điện thoại<br>',
            //             'default'  => 'Messenger Email',
            //         ),
					
			// 		array(
            //             'id'       => 'send-email-testdrive-admin-on-off',
            //             'type'     => 'switch', 
            //             'title'    => __('Send Email Admin?', 'mercedes'),
            //             'subtitle' => __('Look, it\'s on!', 'mercedes'),
            //             'default'  => true,
            //         ),
					
			// 		array(
            //             'id'       => 'admin-email-testdrive-title',
            //             'type'     => 'text', 
            //             'title'    => __('Admin Title Email?', 'mercedes'),
            //             'default'  => 'Title Email',
            //         ),
			// 		array(
            //             'id'       => 'admin-email-testdrive-messenger',
            //             'type'     => 'editor', 
            //             'title'    => __('Admin Messenger Email?', 'mercedes'),
            //             'desc'    => '##name##: Name User Submit.<br> ##email##: Email User Submit<br>##phone##: Số điện thoại<br>',
            //             'default'  => 'Messenger Email',
            //         ),
            //     )
            // );
			
			// $this->sections[] = array(
            //    // 'icon' => ' el-icon-stackoverflow',
            //     'title' => __('Email Contact', 'mercedes'),
            //     'fields' => array(
            //         array(
            //             'id'       => 'email-contact-on-off',
            //             'type'     => 'switch', 
            //             'title'    => __('Email Off?', 'mercedes'),
            //             'subtitle' => __('Look, it\'s on!', 'mercedes'),
            //             'default'  => true,
            //         ),
					
			// 		array(
            //             'id'       => 'email-contact-title',
            //             'type'     => 'text', 
            //             'title'    => __('Title Email?', 'mercedes'),
            //             'default'  => 'Title Email',
            //         ),
			// 		array(
            //             'id'       => 'email-contact-messenger',
            //             'type'     => 'editor', 
            //             'title'    => __('Messenger Email?', 'mercedes'),
            //             'desc'    => '##name##: Name User Submit.<br> ##email##: Email User Submit<br>##phone##: Số điện thoại<br>',
            //             'default'  => 'Messenger Email',
            //         ),
					
			// 		array(
            //             'id'       => 'send-email-contact-admin-on-off',
            //             'type'     => 'switch', 
            //             'title'    => __('Send Email Admin?', 'mercedes'),
            //             'subtitle' => __('Look, it\'s on!', 'mercedes'),
            //             'default'  => true,
            //         ),
					
			// 		array(
            //             'id'       => 'admin-email-contact-title',
            //             'type'     => 'text', 
            //             'title'    => __('Admin Title Email?', 'mercedes'),
            //             'default'  => 'Title Email',
            //         ),
			// 		array(
            //             'id'       => 'admin-email-contact-messenger',
            //             'type'     => 'editor', 
            //             'title'    => __('Admin Messenger Email?', 'mercedes'),
            //             'desc'    => '##name##: Name User Submit.<br> ##email##: Email User Submit<br>##phone##: Số điện thoại<br>',
            //             'default'  => 'Messenger Email',
            //         ),
            //     )
            // );
			//  */
            // ACTUAL DECLARATION OF SECTIONS          
            
            $theme_info = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'zakasubteam') . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'zakasubteam') . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'zakasubteam') . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'zakasubteam') . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';                
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id' => 'redux-opts-1',
                'title' => __('Theme Information 1', 'zakasubteam'),
                'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'zakasubteam')
            );

            $this->args['help_tabs'][] = array(
                'id' => 'redux-opts-2',
                'title' => __('Theme Information 2', 'zakasubteam'),
                'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'zakasubteam')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'zakasubteam');
        }

        /**
          * All the possible arguments for Redux.
          * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name' => 'zaka', // This is where your data is stored in the database and also becomes your global variable name.
                'display_name' => $theme->get('Name'), // Name that appears at the top of your panel
                'display_version' => $theme->get('Version'), // Version that appears at the top of your panel
                'menu_type' => 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu' => true, // Show the sections below the admin menu item or not
                'menu_title' => __('Theme Options', 'zakasubteam'),
                'page' => __('Theme Options', 'zakasubteam'),
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyBM9vxebWLN3bq4Urobnr6tEtn7zM06rEw', // Must be defined to add google fonts to the typography module
                //'admin_bar' => false, // Show the panel pages on the admin bar
                'global_variable' => '', // Set a different name for your global variable other than the opt_name
                'dev_mode' => false, // Show the time the page took to load, etc
                'customizer' => true, // Enable basic customizer support
                // OPTIONAL -> Give you extra features
                'page_priority' => null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent' => 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters               
				'page_permissions' => 'manage_options', // Permissions needed to access the options panel.
                'menu_icon' => '', // Specify a custom URL to an icon
                'last_tab' => '', // Force your panel to always open to a specific tab (by id)
                'page_icon' => 'icon-themes', // Icon displayed in the admin panel next to your menu_title
                'page_slug' => 'omazzz', // Page slug used to denote the panel
                'save_defaults' => true, // On load save the defaults to DB before user clicks save or not
                'default_show' => false, // If true, shows the default value next to each field that is not the default value.
                'default_mark' => '', // What to print by the field's title if the value shown is default. Suggested: *
                // CAREFUL -> These options are for advanced use only
                'transient_time' => 60 * MINUTE_IN_SECONDS,
                'output' => true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag' => true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                //'domain'             	=> 'redux-framework', // Translation domain key. Don't change this unless you want to retranslate all of Redux.
                //'footer_credit'      	=> '', // Disable the footer credit of Redux. Please leave if you can help it.
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database' => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'show_import_export' => false, // REMOVE
                'system_info' => false, // REMOVE
                'help_tabs' => array(),
                'help_sidebar' => '',  
				'show_options_object' => false,
				'forced_dev_mode_off' => false,
            );



            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace("-", "_", $this->args['opt_name']);
                }
                $this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'zakasubteam'), $v);
            } else {
                $this->args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'zakasubteam');
            }

            // Add content after the form.
            $this->args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'zakasubteam');
        }

    }

    new Redux_Framework_sample_config();
}


/*
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):

    function redux_my_custom_field($field, $value) {
        print_r($field);
        print_r($value);
    }

endif;

/*
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):

    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';
        /*
          do your validation

          if(something) {
          $value = $value;
          } elseif(something else) {
          $error = true;
          $value = $existing_value;
          $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }


endif;
