<!-- BEGIN #sidebar -->
  <?php 
    $sidebar_class = "td-pb-span4 td-main-sidebar";
    $post_types = array('mvpv','show','performance');
    if(in_array(get_post_type( get_the_ID()), $post_types) and is_single() )  {
      $sidebar_class = "td-pb-span3 td-main-sidebar";
    }
  ?>
  <div class="<?php echo $sidebar_class; ?>" role="complementary">
    <?php 
      dynamic_sidebar('main-sidebar');
    ?>
     <!-- END .widget -->
  <!-- END #sidebar -->
</div>