<?php
/**
 * Template Name: Show Category Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
 ?>

<?php
get_header(); 
?>    

<div class="td-category-header td-container-wrap">
        <div class="td-container">
            <div class="td-pb-row">
              <div class="td-pb-span12">
                  <div class="td-crumb-container">
                    <div class="entry-crumbs">                   
                      <?php if(function_exists('bcn_display'))
                        {
                          bcn_display();
                        }
                      ?>    
                    </div>
                  </div>
                  <h1 class="entry-title td-page-title"><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></h1>    
              </div>
            </div>
        </div>
</div>
<div class="td-main-content-wrap td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
          <div class="td-pb-span12 td-main-content">
            <div class="td-ss-main-content">
                <div class="td-block-row">
              <?php
                    $show_list_args = array(
                                'hide_empty' => 0,
                                'taxonomy' => 'show_category'
                              );
                    $shows_list = get_terms($show_list_args);
                    if(!empty($shows_list) and !is_wp_error($shows_list)):
                      $count = 0;
                      foreach ($shows_list as $show):
                      $count+=1;
                      $thumbnail = get_term_meta($show->term_id,'thumbnail',true);
                      $thumbnail_url = wp_get_attachment_image_src($thumbnail,'zakasubteam-post-thumbnail');
                      $thumbnail_url = array_shift($thumbnail_url);
                  ?>
                  <div class="td-block-span4">
                      <div class="td_module_3 td_module_wrap td-animation-stack">
                        <div class="td-module-image">
                            <div class="td-module-thumb">

                              <a href="<?php echo get_term_link( $show->slug, $show->taxonomy );?>" rel="bookmark" title="<?php echo $show->name; ?>"><img class="entry-thumb td-animation-stack-type0-2" src="<?php echo $thumbnail_url; ?>" alt="" title="<?php echo $show->name; ?>">
                              </a>
                            </div>
                        </div>
                        <h3 class="entry-title td-module-title"><a href="<?php echo get_term_link( $show->slug, $show->taxonomy );?>" rel="bookmark" title="<?php echo $show->name; ?>"><?php echo $show->name; ?></a></h3>
                        <div class="small_desc"><?php echo substr($show->description,0,200) . "..." ?> <a href="<?php echo get_term_link( $show->slug, $show->taxonomy );?>"> Xem thêm </a></div>
                      </div>
                  </div>
                  <?php 
                    if($count == count($shows_list)) {
                      echo '</div>';
                    } else if ($count %3 ==0) {
                      echo '</div>';
                      echo '<div class="td-block-row">';
                    }
                  ?>
                  <?php 
                  endforeach;
                endif;
                ?>
            </div>
          </div>
        </div>
    </div>

<?php
get_footer(); 
?>