jQuery(document).ready(function($) {

    $('.vid-link').click(function() {
  		var url = $(this).val();
      if(!url) {
         $('#video-frame').css('display','none');
         $('.video-not-found').css('display','block');
      } else {
        $('#video-frame').css('display','block');
        $('.video-not-found').css('display','none');
  		  $('#video-frame').attr('src',url);
      }
  	});



// init Isotope
// var $grid = $('.mvpv-list').isotope({
//   itemSelector: '.item',
//   layoutMode: 'fitRows'
// });
// bind filter button click
$('.single-content').on( 'click', 'a', function() {
  var filterValue = $( this ).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});
// // change is-checked class on buttons
// $('.button-group').each( function( i, buttonGroup ) {
//   var $buttonGroup = $( buttonGroup );
//   $buttonGroup.on( 'click', 'button', function() {
//     $buttonGroup.find('.is-checked').removeClass('is-checked');
//     $( this ).addClass('is-checked');
//   });
// });

//  var swiper = new Swiper('.swiper-container', {
//       pagination: {
//         el: '.swiper-pagination',
//       },
//       navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//       },
//     });

    $("#featured").zAccordion({
      width: 800,
      speed: 600,
      slideClass: "slider",
      slideWidth: 700,
      height: 400,
      animationStart: function(){
        var index = $("#featured li.slider-open").index() + 1;
        console.log(index);
        console.log($( "#thumbs li:nth-child(" + index + ")").find('span').addClass('active-dot'));
        $(".active-dot").removeClass("active-dot");
        $( "#thumbs li:nth-child(" + index + ")").find('span').addClass('active-dot');
      }
    });
    $("#thumbs a").click(function() {
      $("#featured").zAccordion("trigger", $(this).parent().index());
      return false;
    });
    $('#featured li').bind('style', function() {
      console.log($(this).css('height'));
  });

    $(".td-scroll-up-visible").click(function() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    });
    
    $(document).on("click", '.td-next-prev-wrap a',function() {
      var callback= ($(this).data("url-ajax"));
         $.ajax
         ({ 
             url: callback,
             dataType: "json",
             cache: !1,
             success: function(data)
             {
               if(data.content!='') { 
                 $('.show-row').html(data.content);
               }
             }
         });
      });
});

