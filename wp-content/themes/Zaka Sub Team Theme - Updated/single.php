<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
global $post;
if($post->post_type == 'show')
{
	get_template_part('template-parts/single/single','show');
}
else if($post->post_type == 'mvpv') {
	get_template_part('template-parts/single/single','mvpv');
}
else if($post->post_type == 'performance') {
	get_template_part('template-parts/single/single','performance');
}
else{
	get_template_part('template-parts/single/single');
}
?>
<?php get_footer();
