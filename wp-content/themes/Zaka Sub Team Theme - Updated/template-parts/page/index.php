<?php
  global $zaka;
  $play_btn =  isset($zaka['play-btn']) ? $zaka['play-btn']['url'] : '';
?>
<div class="td-main-content-wrap td-main-page-wrap td-container-wrap">
            <div class="tdc-content-wrap">
            <?php
              $show_list_args = array(
                  'hide_empty' => 0,
                  'taxonomy' => 'show_category',
                  'meta_key' => 'home_order',
                  'orderby'    => 'meta_value_num',
                  'order'      => 'ASC',
                  'meta_query' => array(
                        array(
                           'key'       => 'show_home',
                           'value'     => true,
                           'compare'   => '='
                        )
                   )
               );
              $shows_list = get_terms($show_list_args);
              foreach($shows_list as $show):
            ?>
              <?php if ($show->count > 0): ?>
                <div id="<?php echo $show->slug?>" class="tdc-row">
                    <div class="vc_row td_uid_12_<?php echo $show->slug?> wpb_row td-pb-row">
                      <div class="vc_column td_uid_13_<?php echo $show->slug?>  wpb_column vc_column_container tdc-column td-pb-span12">
                          <div class="wpb_wrapper">
                            <div class="td_block_wrap td_block_15 td_uid_14_<?php echo $show->slug?> td-pb-border-top td_block_template_1 td-column-3 td_block_padding" data-td-block-uid="<?php echo $show->slug; ?>">       
                              <div class="td-block-title-wrap">
                                  <h4 class="block-title"><span class="td-pulldown-size" style="margin-right: 0px;"><?php echo $show->name;?></span></h4>
                                </div>
                                <div id="td_uid_14_5b24d1663bd6f" class="td_block_inner td-column-3">
                                  <div class="td-block-row">
                                  <?php
                                        $latest_eps = new WP_Query(array(
                                          "post_type" => "show",
                                          "post_status" => "publish",
                                          "ignore_sticky_posts" => false,
                                          "posts_per_page" => 6,
                                          "tax_query" => array(
                                          "relation" => "AND",
                                            array (
                                                'taxonomy' => 'show_category',
                                                'field' => 'id',
                                                'terms' => $show->term_id,
                                                )
                                            ),
                                        ));
                                        if($latest_eps->have_posts()) : 
                                          while($latest_eps->have_posts()) : $latest_eps->the_post();
                                        ?>
                                          <div class="td-block-span4">
                                        <div class="td_module_mx4 td_module_wrap td-animation-stack">
                                            <div class="td-module-image">
                                              <div class="td-module-thumb">
                                              <a href="<?php echo get_the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                                                <img width="218" height="150" class="entry-thumb td-animation-stack-type0-1 lazy" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" title="<?php the_title(); ?>">
                                              </a>
                                              </div>
                                            </div>
                                            <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                        </div>
                                      </div>
                                      <?php 
                                        endwhile;
                                      endif;
                                      ?>
                                  </div>
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
              <?php endif;?>
            <?php 
              endforeach;
            ?>
            </div>
            <div class="td-container td-pb-article-list">
               <div class="td-pb-row">
                  <div class="td-pb-span8 td-main-content" role="main">
                     <div class="td-ss-main-content td_block_template_1">
                        <div class="clearfix"></div>
                        <div class="td-block-title-wrap">
                           <h4 class="block-title"><span class="td-pulldown-size">MV & PV </span></h4>
                        </div>
                        <div class="td-block-row">
                          <?php
                            if(have_posts()): 
                              $count = 0;
                                while(have_posts()) : the_post();
                              $count+=1;
                           ?>
                           <div class="td-block-span6">
                              <div class="td_module_1 td_module_wrap td-animation-stack">
                                 <div class="td-module-image">
                                    <div class="td-module-thumb"><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title(); ?>"><img width="324" height="160" class="entry-thumb td-animation-stack-type0-1 lazy" src="<?php echo get_the_post_thumbnail_url(get_the_id(),'zakasubteam-post-thumbnail'); ?>" alt="" title="<?php the_title(); ?>"><span class="td-video-play-ico"><img width="40" height="40" class="td-retina td-animation-stack-type0-1" src="<?php echo $play_btn; ?>" alt="video"></span></a></div>
                                    <?php 
                                     $taxs = wp_get_post_terms(get_the_ID(), 'mvpvtype');
                                    ?>
                                    <?php 
                                      foreach($taxs as $tax):
                                    ?>
                                    <a href="<?php echo get_term_link($tax->term_id); ?>" class="td-post-category"><?php echo $tax->name; ?></a> 
                                    <?php 
                                      endforeach;
                                    ?>
                                 </div>
                                 <h3 class="entry-title td-module-title"><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                 <div class="td-module-meta-info">
                                    <time class="entry-date updated td-module-date"><?php the_date(); ?></time></span> 
                                 </div>
                              </div>
                           </div>
                        <?php
                          if($count == $wp_query->post_count) 
                          {
                            echo '</div>';
                          } 
                          else if($count%2==0) {
                            echo '</div>
                            <div class="td-block-row">';
                          }
                        ?>
                        <?php
                          endwhile;
                        endif;
                        ?>
                          

                        <div class="page-nav td-pb-padding-side">
                          <?php
                          $total_pages = $wp_query->max_num_pages;
                          if ($total_pages > 1){
                            $current_page = max(1, get_query_var('paged'));
                    
                            echo paginate_links(array(
                                'base' => get_pagenum_link(1) . '%_%',
                                'format' => '/mvpv/?paged=%#%',
                                'current' => $current_page,
                                'total' => $total_pages,
                                'prev_text'    => __('« prev'),
                                'next_text'    => __('next »'),
                            ));
                        }
                          wp_reset_postdata();
                          ?>

                           <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>

                  
                  <?php get_sidebar(); ?>
               </div>
            </div>

              <div class="td-container td-pb-article-list">
               <div class="td-pb-row">
                  <div class="td-pb-span8 td-main-content" role="main">
                     <div class="td-ss-main-content td_block_template_1">
                        <div class="clearfix"></div>
                        <?php 
                        $latest_films = new WP_Query(array(
                          "post_type" => "film",
                          "post_status" => "publish",
                          "ignore_sticky_posts" => false,
                          "paged" => 1
                        ));
                        ?>
                        <div class="td-block-row">
                          <?php
                            if($latest_films->have_posts()): 
                              ?>
                              <div class="td-block-title-wrap">
                                <h4 class="block-title"><span class="td-pulldown-size">film</span></h4>
                              </div>
                              <?php
                                $count = 0;
                                  while($latest_films->have_posts()) : $latest_films->the_post();
                                    $count+=1;
                              ?>
                           <div class="td-block-span6">
                              <div class="td_module_1 td_module_wrap td-animation-stack">
                                 <div class="td-module-image">
                                    <div class="td-module-thumb"><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title(); ?>"><img width="324" height="160" class="lazy entry-thumb td-animation-stack-type0-1" src="<?php echo get_the_post_thumbnail_url(get_the_id(),'zakasubteam-post-thumbnail'); ?>" alt="" title="<?php the_title(); ?>"><span class="td-video-play-ico">
                                    <img width="40" height="40" class="td-retina td-animation-stack-type0-1" src="<?php echo $play_btn; ?>" alt="video"></span></a></div>
                                 </div>
                                 <h3 class="entry-title td-module-title"><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                 <div class="td-module-meta-info">
                                    <time class="entry-date updated td-module-date"><?php the_date(); ?></time></span> 
                                 </div>
                              </div>
                           </div>
                        <?php
                          if($count == $wp_query->post_count) 
                          {
                            echo '</div>';
                          } 
                          else if($count%2==0) {
                            echo '</div>
                            <div class="td-block-row">';
                          }
                        ?>
                        <?php
                          endwhile;
                        endif;
                        ?>
                          

                        <div class="page-nav td-pb-padding-side">
                          <?php
                          $total_pages = $latest_films->max_num_pages;
                          if ($total_pages > 1){
                            $current_page = max(1, get_query_var('paged'));
                    
                            echo paginate_links(array(
                                'base' => get_pagenum_link(1) . '%_%',
                                'format' => 'film/?paged=%#%',
                                'current' => $current_page,
                                'total' => $total_pages,
                                'prev_text'    => __('« prev'),
                                'next_text'    => __('next »'),
                            ));
                        }
                          wp_reset_postdata();
                          ?>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>

                  
                  <?php get_sidebar(); ?>
               </div>
            </div>

         </div>
