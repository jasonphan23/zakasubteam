<?php
	// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	// $mvpvs = new WP_Query(array(
	// 	"post_type" => "mvpv",
	// 	"post_status" => "publish",
	// 	"ignore_sticky_posts" => false,
	// 	"posts_per_page" => 12,
	// 	"paged" => $paged,
	// 	)); 
		$single_args = array(
 							 'hide_empty' => 0,
 							 'taxonomy' => 'single'
 						);
	  $singles = get_terms($single_args);
	  $queried_object = get_queried_object();
	  global $zaka;
	  $play_btn =  isset($zaka['play-btn']) ? $zaka['play-btn']['url'] : '';
?>

		<div class="td-category-header td-container-wrap">
            <div class="td-container">
               <div class="td-pb-row">
                  <div class="td-pb-span12">
                     <div class="td-crumb-container">
                        <div class="entry-crumbs">
						<?php if(function_exists('bcn_display'))
                  {
                    bcn_display();
                  }
                ?>
						</div>
                     </div>
                     <h1 class="entry-title td-page-title"><h2><a href="#"><?php echo (is_post_type_archive() ? get_queried_object()->label : get_queried_object()->name ); ?></a></h2></h1>
                     
                     
                  </div>
               </div>
            </div>
</div>
       <div class="td-main-content-wrap td-container-wrap">
            <div class="td-container">
               <div class="td-pb-row">

                  <div class="td-pb-span12 td-main-content">
                     <div class="td-ss-main-content"><div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="td-block-row">
                        <?php if(have_posts()) :
                        	$p_count = $GLOBALS['wp_query']->post_count;
                        	$count = 0;
							while(have_posts()) : the_post();
								$count+=1;
						?>             
								<div class="td-block-span4">
		                              <div class="td_module_3 td_module_wrap td-animation-stack">
		                                 <div class="td-module-image">
		                                    <div class="td-module-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img width="324" height="235" class="entry-thumb td-animation-stack-type0-2" src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'zakasubteam-post-thumbnail');?>" alt="" title="<?php the_title(); ?>"><span class="td-video-play-ico"><img width="40" height="40" class="td-retina td-animation-stack-type0-2" src="<?php echo $play_btn; ?>" alt="video"></span></a></div>
		                                 </div>
		                                 <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
		                                 <div class="td-module-meta-info">
		                                   <time class="entry-date updated td-module-date"><?php the_time(); ?></time></span> 
		                                 </div>
		                              </div>
		                        </div>
	                     <?php 
	                     	if($count == $p_count) {
	                     		echo '</div>';
	                     	} else if ($count %3 ==0) {
	                     		echo '</div>';
	                     		echo '<div class="td-block-row">';
	                     	}
	                     ?>
		                 <?php 
							 endwhile;
							else:
						?>
						<div class="alert alert-danger">
                      		<strong>Không tìm thấy!</strong> Không tìm thấy bài viết nào 
                    	</div>
						<?php
		                 endif;
		                 ?>
	                    <div class="clearfix"></div>
                 		<div class="clearfix"></div>
					     <?php 
					      $total = $wp_query->max_num_pages;
					      if ( $total > 1 ) :
					    ?>
				      <div class="page-nav td-pb-padding-side">
				        <?php
				          $total = $wp_query->max_num_pages;
				          $args = apply_filters( 'wpex_pagination_args', array(
				            'base'      => str_replace( 999999999, '%#%', html_entity_decode( get_pagenum_link( 999999999 ) ) ),
				            'format'    => '?paged=%#%',
				            'current'   => max( 1, get_query_var( 'paged') ),
				            'total'     => $total,
				            'mid_size'  => 3,
				            'type'      => 'list',
				          ) );
				    
				          // Output pagination
				          echo paginate_links( $args ); ?>
				      </div>
				      <?php 
				      endif;
				      ?>

		             </div>
                  
               </div>
            </div>
         </div> 
</div>