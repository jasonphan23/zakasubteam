<div class="td-main-content-wrap td-container-wrap">
   <div class="td-container">
      <div class="td-pb-row">
         <div class="td-pb-span12">
            <div class="td-404-title">
               Không tìm thấy
            </div>
            <div class="td-404-sub-title">
              Không tìm thấy nội dung bạn muốn tìm kiếm
            </div>
            <div class="td-404-sub-sub-title">
              Trở về trang chủ <a href="<?php echo home_url(); ?>">Trang chủ </a>
            </div>
            <h4 class="block-title"><span>CÁC BÀI ĐĂNG GẦN ĐÂY</span></h4>
            <div class="td-block-row">
            <?php
                $latest_posts =   new WP_Query(array(
                    "post_type" => array("show","mvpv","film","performance"),
                    "post_status" => "publish",
                    "ignore_sticky_posts" => false,
                    "posts_per_page" => 6,
                    'post_date' => 'DESC'
                ));
                if($latest_posts->have_posts()) :
                    $count = 0;
                    while($latest_posts->have_posts()) : $latest_posts->the_post();
                        $count+=1;
                        $post_type = get_post_type_object(get_post_type(get_the_ID()));

            ?>
               <div class="td-block-span4">
                  <div class="td_module_1 td_module_wrap td-animation-stack">
                     <div class="td-module-image">
                        <div class="td-module-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark" class="td-image-wrap" title="<?php the_title(); ?>"><img width="324" height="160" class="entry-thumb td-animation-stack-type0-2" src="<?php echo get_the_post_thumbnail_url(get_the_ID());?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><span class="td-video-play-ico"><img width="40" height="40" class="td-retina td-animation-stack-type0-2" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/themes/011/images/icons/ico-video-large.png" alt="video"></span></a></div>
                        <a href="<?php the_permalink(); ?>" class="td-post-category"><?php echo $post_type->labels->singular_name; ?></a> 
                     </div>
                     <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                  </div>
               </div>
               <?php 
                  if($count == 6) {
                    echo '</div>';
                  } else if ($count %3 ==0) {
                    echo '</div>';
                    echo '<div class="td-block-row">';
                  }
                  ?>
                <?php 
                endwhile;
                else:
                ?>
                    <div class="alert alert-danger">
                      <strong>Không tìm thấy!</strong> Không tìm thấy video nào 
                    </div>
                  </div>
                <?php
                endif;
                ?>
         </div>
      </div>
   </div>
</div>