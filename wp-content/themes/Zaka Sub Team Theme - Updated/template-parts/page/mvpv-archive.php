<?php
	// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	// $mvpvs = new WP_Query(array(
	// 	"post_type" => "mvpv",
	// 	"post_status" => "publish",
	// 	"ignore_sticky_posts" => false,
	// 	"posts_per_page" => 12,
	// 	"paged" => $paged,
	// 	)); 
		$single_args = array(
 							 'hide_empty' => 0,
 							 'taxonomy' => 'single'
 						);
    $singles = get_terms($single_args);
    $queried_object = get_queried_object();
    global $zaka;
    $play_btn =  isset($zaka['play-btn']) ? $zaka['play-btn']['url'] : '';
?> 
<div class="td-category-header td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span12">
            <div class="td-crumb-container">
              <div class="entry-crumbs">
              <?php if(function_exists('bcn_display'))
                  {
                    bcn_display();
                  }
                ?>
              </div>
              <ul class="td-category">
              <?php
              $tags = get_term_meta($queried_object->term_id,'tags',true);
                if(is_array($tags) and count($tags) > 0 ) : 
                  foreach($tags as $tag):
                    $tag_info = get_term_by('id', intval($tag), 'post_tag');
              ?>
                <li class="entry-category">
                  <a href="<?php echo get_term_link( intval($tag), 'post_tag' );?>">
                      <?php echo  $tag_info->name; ?>
                  </a>
              <?php 
                  endforeach;
               endif;
              ?>
              </li>
            </ul>
            </div>
            <h1 class="entry-title td-page-title"><h2><a href="#"><?php echo (is_post_type_archive() ? get_queried_object()->label : get_queried_object()->name ); ?></a></h2></h1>                    
        </div>
      </div>
  </div>
</div>

<div class="td-main-content-wrap td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span9 td-main-content">
            <div class="td-ss-main-content"><div class="clearfix"></div>
              <div class="clearfix"></div>
              <div class="td-block-row">
              <?php if(have_posts()) :
                $p_count = $GLOBALS['wp_query']->post_count;
                $count = 0;
                while(have_posts()) : the_post();
                  $count+=1;
                ?>             
								<div class="td-block-span4">
                  <div class="td_module_3 td_module_wrap td-animation-stack">
                      <div class="td-module-image">
                        <div class="td-module-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img width="324" height="235" class="entry-thumb td-animation-stack-type0-2" src="<?php echo get_the_post_thumbnail_url(get_the_ID());?>" alt="" title="<?php the_title(); ?>"><span class="td-video-play-ico"><img width="40" height="40" class="td-retina td-animation-stack-type0-2" src="<?php echo $play_btn; ?>" alt="video"></span></a></div>
                      </div>
                      <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                      <div class="td-module-meta-info">
                        <time class="entry-date updated td-module-date"><?php the_time(); ?></time></span> 
                      </div>
                  </div>
		             </div>
                  <?php 
                  if($count == $p_count) {
                    echo '</div>';
                  } else if ($count %3 ==0) {
                    echo '</div>';
                    echo '<div class="td-block-row">';
                  }
                  ?>
                <?php 
                endwhile;
                else:
                ?>
                    <div class="alert alert-danger">
                      <strong>Không tìm thấy!</strong> Không tìm thấy video nào 
                    </div>
                  </div>
                <?php
                endif;
                ?>
              <div class="clearfix"></div>
              <div class="clearfix"></div>
              <?php 
                $total = $wp_query->max_num_pages;
                if ( $total > 1 ) :
              ?>
                <div class="page-nav td-pb-padding-side">
                  <?php
                    $total = $wp_query->max_num_pages;
                    $args = apply_filters( 'wpex_pagination_args', array(
                      'base'      => str_replace( 999999999, '%#%', html_entity_decode( get_pagenum_link( 999999999 ) ) ),
                      'format'    => '?paged=%#%',
                      'current'   => max( 1, get_query_var( 'paged') ),
                      'total'     => $total,
                      'mid_size'  => 3,
                      'type'      => 'list',
                    ) );
              
                    // Output pagination
                    echo paginate_links( $args ); ?>
                </div>
                <?php 
                  endif;
                ?>
		     </div>  
       </div>
        <div class="td-pb-span3 td-main-sidebar" role="complementary">
          <div class="td-ss-main-sidebar" style="width: auto; position: static; top: auto; bottom: auto; z-index: 1;">
            <div class="clearfix"></div>
            <div class="td_block_wrap td_block_social_counter td_block_widget td_uid_34_5b24d16641c51_rand td-social-style3 td-social-colored td-pb-border-top td_block_template_1">             
                <div class="td-block-title-wrap">
                  <h4 class="block-title"><span class="td-pulldown-size">Stay connected</span></h4>
                </div>
                <div class="td-social-list">
                  <div class="td_social_type td-pb-margin-side td_social_facebook">
                      <div class="td-social-box">
                        <div class="td-sp td-sp-facebook"></div>
                        <span class="td_social_info">10,222</span><span class="td_social_info td_social_info_name">Fans</span><span class="td_social_button"><a href="https://www.facebook.com/tagDiv" target="_blank">Like</a></span>
                      </div>
                  </div>
                  <div class="td_social_type td-pb-margin-side td_social_googleplus">
                      <div class="td-social-box">
                        <div class="td-sp td-sp-googleplus"></div>
                        <span class="td_social_info">309</span><span class="td_social_info td_social_info_name">Followers</span><span class="td_social_button"><a href="https://plus.google.com/+tagdivThemes" target="_blank">Follow</a></span>
                      </div>
                  </div>
                  <div class="td_social_type td-pb-margin-side td_social_instagram">
                      <div class="td-social-box">
                        <div class="td-sp td-sp-instagram"></div>
                        <span class="td_social_info">7,504,741</span><span class="td_social_info td_social_info_name">Followers</span><span class="td_social_button"><a href="http://instagram.com/love_food#" target="_blank">Follow</a></span>
                      </div>
                  </div>
                  <div class="td_social_type td-pb-margin-side td_social_youtube">
                      <div class="td-social-box">
                        <div class="td-sp td-sp-youtube"></div>
                        <span class="td_social_info">5,933</span><span class="td_social_info td_social_info_name">Subscribers</span><span class="td_social_button"><a href="http://www.youtube.com/tagDiv" target="_blank">Subscribe</a></span>
                      </div>
                  </div>
                </div>
            </div>
            <div class="td_block_wrap td_block_popular_categories td_block_widget td_uid_35_5b24d16641d85_rand widget widget_categories td-pb-border-top td_block_template_1" data-td-block-uid="td_uid_35_5b24d16641d85">
                <div class="td-block-title-wrap">
                  <h4 class="block-title"><span class="td-pulldown-size">RECIPE CATEGORIES</span></h4>
                </div>
                <ul class="td-pb-padding-side">
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/recipe-of-the-day/">Recipe of The Day<span class="td-cat-no">10</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/recipes/chicken-beef/">Chicken &amp; Beef<span class="td-cat-no">7</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/recipes/pasta/">Pasta<span class="td-cat-no">7</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/recipes/deserts/">Deserts<span class="td-cat-no">6</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/recipes/salads/">Salads<span class="td-cat-no">6</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/recipes/smoothies/">Smoothies<span class="td-cat-no">6</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/kids-menu/">Kids Menu<span class="td-cat-no">6</span></a></li>
                  <li><a href="https://demo.tagdiv.com/newspaper_recipes/category/quick-and-easy-recipes/">Quick and Easy Recipes<span class="td-cat-no">6</span></a></li>
                </ul>
            </div>
            <div class="td_block_wrap td_block_7 td_block_widget td_uid_36_5b24d16642200_rand td-pb-border-top td_block_template_1 td-column-1 td_block_padding" data-td-block-uid="td_uid_36_5b24d16642200">
                <div class="td-block-title-wrap">
                  <h4 class="block-title"><span class="td-pulldown-size">RECIPE OF THE DAY</span></h4>
                </div>
                <div id="td_uid_36_5b24d16642200" class="td_block_inner">
                  <div class="td-block-span12">
                      <div class="td_module_6 td_module_wrap td-animation-stack">
                        <div class="td-module-thumb"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-sushi-roll/" rel="bookmark" title="Sushi Roll"><img width="100" height="70" class="entry-thumb td-animation-stack-type0-1" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/uploads/2016/03/41-100x70.jpg" alt="" title="Sushi Roll"></a></div>
                        <div class="item-details">
                            <h3 class="entry-title td-module-title"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-sushi-roll/" rel="bookmark" title="Sushi Roll">Sushi Roll</a></h3>
                            <div class="td-module-meta-info">
                              <a href="https://demo.tagdiv.com/newspaper_recipes/category/quick-and-easy-recipes/" class="td-post-category">Quick and Easy Recipes</a> <span class="td-post-date"><time class="entry-date updated td-module-date" datetime="2016-03-25T12:48:46+00:00">March 25, 2016</time></span> 
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="td-block-span12">
                      <div class="td_module_6 td_module_wrap td-animation-stack">
                        <div class="td-module-thumb"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-rocky-road-bites/" rel="bookmark" title="Rocky Road Bites"><img width="100" height="70" class="entry-thumb td-animation-stack-type0-1" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/uploads/2016/03/38-100x70.jpg" alt="" title="Rocky Road Bites"></a></div>
                        <div class="item-details">
                            <h3 class="entry-title td-module-title"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-rocky-road-bites/" rel="bookmark" title="Rocky Road Bites">Rocky Road Bites</a></h3>
                            <div class="td-module-meta-info">
                              <a href="https://demo.tagdiv.com/newspaper_recipes/category/kids-menu/" class="td-post-category">Kids Menu</a> <span class="td-post-date"><time class="entry-date updated td-module-date" datetime="2016-03-25T12:48:38+00:00">March 25, 2016</time></span> 
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="td-block-span12">
                      <div class="td_module_6 td_module_wrap td-animation-stack">
                        <div class="td-module-thumb"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-apple-pumpkin-muffins/" rel="bookmark" title="Apple Pumpkin Muffins"><img width="100" height="70" class="entry-thumb td-animation-stack-type0-1" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/uploads/2016/03/34-100x70.jpg" alt="" title="Apple Pumpkin Muffins"></a></div>
                        <div class="item-details">
                            <h3 class="entry-title td-module-title"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-apple-pumpkin-muffins/" rel="bookmark" title="Apple Pumpkin Muffins">Apple Pumpkin Muffins</a></h3>
                            <div class="td-module-meta-info">
                              <a href="https://demo.tagdiv.com/newspaper_recipes/category/kids-menu/" class="td-post-category">Kids Menu</a> <span class="td-post-date"><time class="entry-date updated td-module-date" datetime="2016-03-25T12:48:29+00:00">March 25, 2016</time></span> 
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="td-block-span12">
                      <div class="td_module_6 td_module_wrap td-animation-stack">
                        <div class="td-module-thumb"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-frozen-blood-and-sand-cocktail/" rel="bookmark" title="Frozen Blood and Sand Cocktail"><img width="100" height="70" class="entry-thumb td-animation-stack-type0-1" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/uploads/2016/03/32-100x70.jpg" alt="" title="Frozen Blood and Sand Cocktail"></a></div>
                        <div class="item-details">
                            <h3 class="entry-title td-module-title"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-frozen-blood-and-sand-cocktail/" rel="bookmark" title="Frozen Blood and Sand Cocktail">Frozen Blood and Sand Cocktail</a></h3>
                            <div class="td-module-meta-info">
                              <a href="https://demo.tagdiv.com/newspaper_recipes/category/recipe-of-the-day/" class="td-post-category">Recipe of The Day</a> <span class="td-post-date"><time class="entry-date updated td-module-date" datetime="2016-03-25T12:48:23+00:00">March 25, 2016</time></span> 
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="td-block-span12">
                      <div class="td_module_6 td_module_wrap td-animation-stack">
                        <div class="td-module-thumb"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-red-white-and-blue-protein-smoothie/" rel="bookmark" title="Red, White and Blue Protein Smoothie"><img width="100" height="70" class="entry-thumb td-animation-stack-type0-1" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/uploads/2016/03/29-100x70.jpg" alt="" title="Red, White and Blue Protein Smoothie"></a></div>
                        <div class="item-details">
                            <h3 class="entry-title td-module-title"><a href="https://demo.tagdiv.com/newspaper_recipes/td-post-red-white-and-blue-protein-smoothie/" rel="bookmark" title="Red, White and Blue Protein Smoothie">Red, White and Blue Protein Smoothie</a></h3>
                            <div class="td-module-meta-info">
                              <a href="https://demo.tagdiv.com/newspaper_recipes/category/recipe-of-the-day/" class="td-post-category">Recipe of The Day</a> <span class="td-post-date"><time class="entry-date updated td-module-date" datetime="2016-03-25T12:48:16+00:00">March 25, 2016</time></span> 
                            </div>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="clearfix"></div>
          </div>
      </div>

    </div>
  </div> 
</div>
               
