<?php
  $first_ep_link = "not found";
  $queried_object = get_queried_object();
  // echo json_encode($queried_object);
  $first_ep = new WP_Query(array(
      "post_type" => "show",
			"post_status" => "publish",
			"ignore_sticky_posts" => false,
			"posts_per_page" => 1,
      'meta_key' => 'episode',
      'orderby'    => 'meta_value_num',
      'order'      => 'ASC',
			"tax_query" => array(
				"relation" => "AND",
        array (
      					'taxonomy' => 'show_category',
      					'field' => 'id',
      					'terms' => $queried_object->term_id,
      				)
			),
  ));
  $thumbnail = get_term_meta($queried_object->term_id,'thumbnail',true);
  $thumbnail_url = wp_get_attachment_image_src($thumbnail,'zakasubteam-post-thumbnail');
  $thumbnail_url = array_shift($thumbnail_url);
if($first_ep->have_posts()):
  while ( $first_ep->have_posts() ) : $first_ep->the_post();
    $first_ep_link = get_permalink(get_the_ID());
  endwhile;
endif;
global $zaka;
$play_btn =  isset($zaka['play-btn']) ? $zaka['play-btn']['url'] : '';
?>

<div class="td-category-header td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
      
        <div class="td-pb-span12">
            <div class="td-crumb-container">
              <div class="entry-crumbs">                   
              <?php if(function_exists('bcn_display'))
                  {
                    bcn_display();
                  }
                ?>       
              </div>
            </div>
            <ul class="td-category">
              <?php
              $tags = get_term_meta($queried_object->term_id,'tags',true);
                if(is_array($tags) and count($tags) > 0 ) : 
                  foreach($tags as $tag):
                    $tag_info = get_term_by('id', intval($tag), 'post_tag');
              ?>
                <li class="entry-category">
                  <a href="<?php echo get_term_link( intval($tag), 'post_tag' );?>">
                      <?php echo  $tag_info->name; ?>
                  </a>
              <?php 
                  endforeach;
               endif;
              ?>
              </li>
            </ul>
            <h1 class="entry-title td-page-title">
              <h2>
                <a href="<?php ?>">
                  <?php echo $queried_object->name;  ?>
                </a>
              </h2>
            </h1>
            <div class="td-header-sp-logo">
              <h1 class="td-logo"> 
                <a class="td-main-logo" href="<?php the_permalink(); ?>">
                  <img class="show-cover" src="<?php echo wp_get_attachment_url(intval(get_term_meta($queried_object->term_id,'cover_image',true)));?>" alt="">
                  <span class="td-visual-hidden"><?php the_title() ; ?></span>
                </a>
              </h1>
            </div>                    
        </div>
      </div>
  </div>
</div>
<div class="td-main-content-wrap td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span4 td-main-content">
            <div class="td-block-span12">
              <div class="td_module_1 td_module_wrap td-animation-stack">
                  <div class="td-module-image">
                      <div class="td-module-thumb">
                        <a href="" rel="bookmark" title="<?php $queried_object->name; ?>">
                          <img class="entry-thumb td-animation-stack-type0-2" src="<?php echo $thumbnail_url  ?>" alt="" title="<?php $queried_object->name; ?>">
                        </a>
                      </div>
                   </div>
                    <h4 class="block-title">
                      <a href="<?php echo $first_ep_link; ?>"><i class="far fa-play-circle"></i> XEM PHIM </a>
                    </h4>
              </div>
          </div>
        </div>

        <div class="td-pb-span8">
            <h3 class="movie-title"><?php echo $queried_object->name; ?></h3>
            <hr>
            <div class="movie-content">
              <p class="movie-info">
                <label>Ngày công chiếu: </label>
                <?php echo get_term_meta($queried_object->term_id,'starting_date',true); ?>
              </p>
              <p class="movie-info">
                <label>Thể loại: </label>
                <?php $genres = get_term_meta($queried_object->term_id,'genre',true);
                  foreach($genres as $genre):
                    $genre_info = get_term_by('id', intval($genre), 'show_genre');
                ?>
                  <a href="<?php echo get_term_link( intval($genre), 'show_genre' );?>">
                    <?php echo  $genre_info->name; ?>
                  </a>
                  <?php endforeach; ?>
              </p>
              <p class="movie-info">
                <label>Số tập: </label>
                  <?php echo  $queried_object->count; ?>
              </p>

              <p class="movie-info">
                <label>Độ dài: </label>
              <?php echo get_term_meta($queried_object->term_id,'length',true); ?>
              </p>
              <div class="description-wrapper">
                <div class="description collapsed">
                <?php echo $queried_object->description; ?>
                </div>
                </div>
            </div>
        </div>

        </div>
    </div>
  </div> 
</div>

