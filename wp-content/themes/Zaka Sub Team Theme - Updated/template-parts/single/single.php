<?php
if ( have_posts() ) :
  the_post();
  //INCREASE VIEW NUMBER
  $view_num = get_post_meta(get_the_ID(),'view_num',true);
  if($view_num)
  {
  	update_post_meta(get_the_ID(),'view_num',$view_num+1);
  } else {
  	update_post_meta(get_the_ID(),'view_num',1);
  }
  $post_type = get_post_type_object(get_post_type(get_the_ID()));
  $other_posts = new WP_Query(array(
    "post_type" => get_post_type(get_the_ID()),
    "post_status" => "publish",
    "ignore_sticky_posts" => false,
    "posts_per_page" => 6,
     'post__not_in' => array(get_the_ID())
   ));
?>

<div class="td-category-header td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span12">
            <div class="td-crumb-container">
              <div class="entry-crumbs">  
                <?php if(function_exists('bcn_display'))
                  {
                    bcn_display();
                  }
                ?>
              </div>
            </div>
            <ul class="td-category">
              <?php
                $tags = wp_get_post_tags(get_the_ID());
                if(is_array($tags) and count($tags) > 0 ) : 
                  foreach($tags as $tag):
              ?>
                <li class="entry-category">
                  <a href="<?php echo get_tag_link($tag->term_id);?>">
                      <?php echo  $tag->name; ?>
                  </a>
              <?php 
                  endforeach;
                endif;
              ?>
              </li>
            </ul>
            <h1 class="entry-title td-page-title"><h2><a href="#"><?php echo the_title(); ?></a></h2></h1>                    

        </div>
      </div>
  </div>
</div>

<div class="td-main-content-wrap td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span8 td-main-content">
            <div class="td-ss-main-content">
                  <div class="td-post-content">
                    <?php the_content(); ?>
                  </div>
                  <div class="td_block_wrap td_block_related_posts">
                    <h4 class="block-title"><span class="td-pulldown-size">Các <?php echo $post_type->label; ?> khác</span></h4>
                    <div class="td_block_inner">
                        <div class="td-related-row">
                        <?php if($other_posts->have_posts()) : 
                          while($other_posts->have_posts()) : $other_posts->the_post();
                        ?>
                          <div class="td-related-span4">
                              <div class="td_module_related_posts td-animation-stack td_mod_related_posts">
                                <div class="td-module-image">
                                    <div class="td-module-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img width="218" height="150" class="entry-thumb td-animation-stack-type0-1" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" title="<?php the_title(); ?>"></a></div>
                                </div>
                                <div class="item-details">
                                    <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                </div>
                              </div>
                          </div>
                          <?php
                          endwhile;
                        endif;
                          ?>
                        </div>
                    </div>
                    <div class="td-next-prev-wrap"><a href="#" class="td-ajax-prev-page ajax-page-disabled" id="prev-page-td_uid_14_5b08375d2367e" data-td_block_id="td_uid_14_5b08375d2367e"><i class="td-icon-font td-icon-menu-left"></i></a><a href="#" class="td-ajax-next-page" id="next-page-td_uid_14_5b08375d2367e" data-td_block_id="td_uid_14_5b08375d2367e"><i class="td-icon-font td-icon-menu-right"></i></a></div>
                  </div>
              </div>
       </div>
         <?php get_sidebar(); ?>
    </div>
  </div> 
</div>

<?php 
  endif;
?>