<?php
if ( have_posts() ) {
  the_post();
  //INCREASE VIEW NUMBER
  $view_num = get_post_meta(get_the_ID(),'view_num',true);
  if($view_num)
  {
  	update_post_meta(get_the_ID(),'view_num',$view_num+1);
  } else {
  	update_post_meta(get_the_ID(),'view_num',1);
  }

 //GET OTHER MVPVs
 $other_mvs = new WP_Query(array(
	"post_type" => "mvpv",
	"post_status" => "publish",
	"ignore_sticky_posts" => false,
	"posts_per_page" => 6,
	'post__not_in' => array(get_the_ID())
));
 $gg_drive_url = get_post_meta(get_the_ID(),'gg_drive_url',true);
}
?>

<div class="td-category-header td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span12">
            <div class="td-crumb-container">
              <div class="entry-crumbs">                   
                <?php if(function_exists('bcn_display'))
                    {
                      bcn_display();
                    }
                ?>             
              </div>
            </div>
            <ul class="td-category">
              <?php
                $tags = wp_get_post_tags(get_the_ID());
                if(is_array($tags) and count($tags) > 0 ) : 
                  foreach($tags as $tag):
              ?>
                <li class="entry-category">
                  <a href="<?php echo get_tag_link($tag->term_id);?>">
                      <?php echo  $tag->name; ?>
                  </a>
              <?php 
                  endforeach;
                endif;
              ?>
              </li>
            </ul>
            <h1 class="entry-title td-page-title"><h2><a href="#"><?php echo the_title(); ?></a></h2></h1>                    
        </div>
      </div>
  </div>
</div>

<div class="td-main-content-wrap td-container-wrap">
  <div class="td-container">
      <div class="td-pb-row">
        <div class="td-pb-span9 td-main-content">
            <div class="td-ss-main-content">
             	 <div class="video-wrap">
                <?php if($gg_drive_url):?>
                  <iframe src="<?php echo  $gg_drive_url; ?>" width="780" height="480" id="video-frame" allowfullscreen allow="autoplay">
                  </iframe>
              <?php else: ?>
              <div class="alert alert-danger">
                       <strong>Không tìm thấy Video !</strong> Không tìm thấy Video xin vui lòng thử lại sau , hoặc thử lại với đường link khác
                  </div>
                <?php endif; ?>
                </div>
                <div class="td-view-vid-in">
                    <p><b>Xem tại</b></p>
                    <div class="td-view-vid-btn">
                      <button type="button" class="btn btn-success vid-link" id="ggdrive-vid-link" value='<?php echo  $gg_drive_url; ?>'>GG Drive</button>
                      <button type="button" class="btn btn-primary vid-link" id="dailymotion-vid-link"  value='<?php echo  $dailymotion_url; ?>'>Dailymotion</button>
                      <button type="button" class="btn btn-info vid-link" id="4file-vid-link" value='<?php echo  $fourfile_url; ?>'>4File</button>
                    </div>
                  </div>
                  <div class="td-post-content">
                    <?php the_content(); ?>
                  </div>
                  <div class="td_block_wrap td_block_related_posts">
                    <h4 class="block-title"><span class="td-pulldown-size">Các MV & PV khác</span></h4>
                    <div class="td_block_inner">
                        <div class="td-related-row">
                        <?php if($other_mvs->have_posts()) : 
                            $count = 0 ;
                          while($other_mvs->have_posts()) : $other_mvs->the_post();
                            $count+=1;
                        ?>
                          <div class="td-related-span4">
                              <div class="td_module_related_posts td-animation-stack td_mod_related_posts">
                                <div class="td-module-image">
                                    <div class="td-module-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img width="218" height="150" class="entry-thumb td-animation-stack-type0-1" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" title="<?php the_title(); ?>"></a></div>
                                </div>
                                <div class="item-details">
                                    <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                </div>
                                </div>
                          </div>
                           <?php 
                            if($count == 6) {
                              echo '</div>';
                            } else if ($count %3 ==0) {
                              echo '</div>';
                              echo '<div class="td-related-row">';
                            }
                            ?>
                          <?php 
                            endwhile;
                          endif;
                          ?>
                        </div>
                    </div>
                    <div class="td-next-prev-wrap"><a href="#" class="td-ajax-prev-page ajax-page-disabled" id="prev-page-td_uid_14_5b08375d2367e" data-td_block_id="td_uid_14_5b08375d2367e"><i class="td-icon-font td-icon-menu-left"></i></a><a href="#" class="td-ajax-next-page" id="next-page-td_uid_14_5b08375d2367e" data-td_block_id="td_uid_14_5b08375d2367e"><i class="td-icon-font td-icon-menu-right"></i></a></div>
                  </div>
              </div>
       </div>
       <?php get_sidebar(); ?>

    </div>
  </div> 
</div>

