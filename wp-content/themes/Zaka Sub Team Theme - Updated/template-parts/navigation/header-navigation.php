<?php
    global $zaka;
    $header_lg =  isset($zaka['header-logo']) ? $zaka['header-logo']['url'] : '';
    $header_bgd =  isset($zaka['header-bgd']) ? $zaka['header-bgd']['url'] : '';
    $site_title =  isset($zaka['site-title']) ? $zaka['site-title']: '';
?>
<div class="td-banner-wrap-full td-logo-wrap-full td-container-wrap ">
<div class="td-container td-header-row td-header-header">
<div class="td-header-sp-logo">
<h1 class="td-logo"> <a class="td-main-logo" href="<?php echo home_url(); ?>">
<img class="td-retina-data"
 src="<?php echo $header_lg; ?>?" 
 alt="<?php echo $site_title; ?>" title="<?php echo $site_title; ?>">
</a>
</h1> </div>

</div>
</div>
<div class="td-header-menu-wrap-full td-container-wrap ">
    <div class="td-header-menu-wrap td-header-gradient" >
        <div class="td-container td-header-row td-header-main-menu">
            <div id="td-header-menu" role="navigation">
            <div id="td-top-mobile-toggle"><a href="#"><i class="fas fa-bars"></i></a></div>
            <div class="td-main-menu-logo td-logo-in-header">
                <a class="td-mobile-logo td-sticky-disable" href="<?php echo home_url(); ?>">
                <img src="<?php echo $header_lg; ?>" alt="<?php echo $site_title; ?>">
                </a>
                <a class="td-header-logo td-sticky-disable" href="<?php echo home_url(); ?>">
                <img src="<?php echo $header_lg; ?>" alt="<?php echo $site_title; ?>">
                </a>
            </div>
                <?php
                $zakawalker = new Zakasubteam_Nav_Walker;
                wp_nav_menu( array(
                    'theme_location' => 'top',
                    'menu_class' => 'sf-menu sf-js-enabled',
                    'container' => 'div',
                    'container_class' => 'menu-main-menu-container',
                    'walker' => $zakawalker
                ) ); 
                ?>
            </div>
            <div class="header-search-wrap">
            <div class="td-search-btns-wrap">
                <a id="td-header-search-button" href="#" role="button" class="dropdown-toggle " data-toggle="dropdown"><i class="fa fa-search"></i></a>
                <a id="td-header-search-button-mob" href="#" role="button" class="dropdown-toggle " data-toggle="dropdown"><i class="fa fa-search"></i></a>
            </div>
            <div class="td-drop-down-search" aria-labelledby="td-header-search-button">
                <?php echo get_search_form()?>
                <div id="td-aj-search"></div>
            </div>
            </div>
        </div>
    </div>
</div>
