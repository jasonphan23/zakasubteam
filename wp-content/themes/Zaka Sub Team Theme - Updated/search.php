<?php
   get_header();
   	// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
   	// $mvpvs = new WP_Query(array(
   	// 	"post_type" => "mvpv",
   	// 	"post_status" => "publish",
   	// 	"ignore_sticky_posts" => false,
   	// 	"posts_per_page" => 12,
   	// 	"paged" => $paged,
   	// 	)); 
   
   ?> 
<div class="td-main-content-wrap td-container-wrap">
   <div class="td-container ">
      <div class="td-crumb-container">
         <div class="entry-crumbs">
            <?php if(function_exists('bcn_display'))
                      {
                        bcn_display();
                      }
            ?>
		 </div>
      </div>
      <div class="td-pb-row">
         <div class="td-pb-span8 td-main-content">
            <div class="td-ss-main-content">
               <div class="clearfix"></div>
               <div class="td-page-header">
                  <h1 class="entry-title td-page-title">
                     <span class="td-search-query"><?php echo $_GET['s']; ?></span> - <span> Kết quả tìm kiếm </span>
                  </h1>
                  <div class="search-page-search-wrap">
                     <form method="get" class="td-search-form-widget" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div role="search">
                           <input class="td-widget-search-input" type="text" value="<?php echo $_GET['s'] ; ?>" name="s" id="s"><input class="wpb_button wpb_btn-inverse btn" type="submit" id="searchsubmit" value="Search">
                        </div>
                     </form>
                  </div>
			   </div>
			   <?php if(have_posts()) : 
				  		while(have_posts()) : the_post();
				?>
               <div class="td_module_16 td_module_wrap td-animation-stack">
					<div class="td-module-thumb">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
							<img width="300" height="160" class="entry-thumb td-animation-stack-type0-2" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" title="<?php the_title(); ?>">
								<span class="td-video-play-ico">
									<img width="40" height="40" class="td-retina td-animation-stack-type0-2" src="https://demo.tagdiv.com/newspaper_recipes/wp-content/themes/011/images/icons/ico-video-large.png" alt="video">
								</span>
						</a>
					</div>
					<div class="item-details">
						<h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
						<div class="td-excerpt">
							<?php the_excerpt(); ?>
						</div>
					</div>
			   </div>
			   <?php 
					 endwhile;
					endif;  
			   ?>
               <div class="clearfix"></div>
			   <?php 
			   	global $wp_query;
                $total = $wp_query->max_num_pages;
                if ( $total > 1 ) :
              ?>
                <div class="page-nav td-pb-padding-side">
                  <?php
                    $total = $wp_query->max_num_pages;
                    $args = apply_filters( 'wpex_pagination_args', array(
                      'base'      => str_replace( 999999999, '%#%', html_entity_decode( get_pagenum_link( 999999999 ) ) ),
                      'format'    => '?paged=%#%',
                      'current'   => max( 1, get_query_var( 'paged') ),
                      'total'     => $total,
                      'mid_size'  => 3
                    ) );
              
                    // Output pagination
                    echo paginate_links( $args ); ?>
                </div>
                <?php 
                  endif;
                ?>
            </div>
         </div>
		 <?php get_sidebar(); ?>
      </div>
   </div>
</div>
<?php
   get_footer();
   ?>