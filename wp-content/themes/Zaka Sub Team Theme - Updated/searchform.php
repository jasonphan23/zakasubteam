<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>


 <form method="get" class="td-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<div role="search" class="td-head-form-search-wrap">
			<input id="td-header-search" type="text" value="" name="s" autocomplete="off"><input class="wpb_button wpb_btn-inverse btn" type="submit" id="td-header-search-top" value="Search">
		</div>
  </form>