<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'zakasubteam');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A)@/maNJ/$S13g_c)wsXWL0KF()SiCjKRC^1;0bp8L@ jEO#<VjxHn(`WJY#dI,0');
define('SECURE_AUTH_KEY',  ']gT*h<L:GH4OY3G/VKl{o]k$[Ta6PYs=23w0=E:MglajEve$G=LL-Lzf4YYR_?Wh');
define('LOGGED_IN_KEY',    'Kb56Wa!;7p.&4(ZkQoVYtI/%^ge|%9VY|#u)3^u.)}oIp5eT_RH}c9ATy*?3R7qd');
define('NONCE_KEY',        'cVtd/x_7|(>HVWXwnIf::CXepHwDJ5YH?bEJz=Yo[9{A.PS/Gtdt}n=*&N-)1(&m');
define('AUTH_SALT',        '+j`p?q-PCO0q>n+L&2L5Vc/?CYc%d/riBgC;*~IkSxbimqfBI^z:6`<7p(d4%<*?');
define('SECURE_AUTH_SALT', '@[O/Kpc`N|PI%it;^*Pxo!F14>#/K!:Oi]MaGp96hg[IS@`@atd.C5y{DS Fql#x');
define('LOGGED_IN_SALT',   'r:0F/QpkY^E68(Z}CM:HI$:sa!zT>$]p||rV1^MJV`n#RJRdAnL>H]`$ABxhLcsF');
define('NONCE_SALT',       'oDnMWkhD>gGK~?`63R[GK36VE4>40e3{RHbiY0ri#V&JOxb`c7VP7?bL%]P8o%*m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
